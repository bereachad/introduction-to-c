#include<iostream>
#include<cmath>
#include <fstream>

using namespace std;


class studentgrades {

public:
 studentgrades();
  // default constructor which sets:
  // grade to ' ' overallcomputed to false
  // and sets all other private member variables to 0

  int get_studentID() const;

  int get_exam1() const;
  // accessor which returns the value of exam1

  int get_exam2() const;
  // accessor which returns the value of exam2

  int get_final() const;
  // accessor which returns the value of final

  double get_overall() const;
  // accessor which returns the value of overall

  char get_grade() const;
  // accessor which returns the value of grade

  void set_studentID(int id);

  void set_exam1(int exam1grade);
  // mutator which sets the value of exam1 to exam1grade

  void set_exam2(int exam2grade);
  // mutator which sets the value of exam2 to exam2grade

  void set_final(int finalgrade);
  // mutator which sets the value of final to finalgrade

  void computegrade();
 // mutator which sets overall, grade, and overallcomputed as follows:
 // overall = 30%*exam1 + 30%*exam2 + 40%*final;
 // grade is set based upon the value of overall as follows:
 //    grade = 'A' if overall >=90.0
 //    grade = 'B' if 90.0 < overall <= 80.0
 //    grade = 'C' if 80.0 < overall <= 70.0
 //    grade = 'D' if 70.0 < overall <= 60.0
 //    grade = 'F' if overall < 60.0
 // overallcomputed = true

 void output(ostream& outs);

  void reset_all();
  // mutator which sets grade to ' ', overallcomputed to false
  // and all other private member variables to 0

private:

  int studentID;
  int exam1;
  int exam2;
  int final;
  double overall;
  bool overallcomputed;
  char grade;

}; // Recall that the class definition must end with a semicolon.
int main()
{
    int numStudents(0),e1,e2,e3,final, studentID;
    char oFileName[20],check;

    ofstream outstream;
    cout<<"How many students are in your class professor? ";
    cin>>numStudents;

    studentgrades student;

    cout<<"\nWhat is the name of the output file?";
    cin>>oFileName;

    outstream.open(oFileName);
    outstream<<"StudentID\t"<<"Exam 1\t"<<"Exam 2\t"<<"Final\t"<<"Overall\t"<<"Grade"<<endl;

    for (int x=1; x<=numStudents; x++)
    {
        cout<<"\nEnter Student ID number:";
        cin>>studentID;
        student.set_studentID(studentID);
        cout<<"Enter exam 1 grade:";
        cin>>e1;
        student.set_exam1(e1);
         cout<<"Enter exam 2 grade:";
        cin>>e2;
        student.set_exam2(e2);
         cout<<"Enter Final grade:";
        cin>>final;
        student.set_final(final);
        student.computegrade();
        char grade=student.get_grade();
        e1=student.get_exam1();
        e2=student.get_exam2();
        final=student.get_final();
        studentID=student.get_studentID();
        int overall=student.get_overall();

        student.output(cout);
        cout<<"\n Is this correct? ( Y/ N)";
        cin>>check;
            if (check=='y'||check=='Y')
                student.output(outstream);
        student.reset_all();
    }

system("PAUSE");
return 0;
}

studentgrades::studentgrades()
{
    exam1=0;
    exam2=0;
    final=0;
    grade=' ';
    studentID=0;
}


   void studentgrades::set_exam1(int exam1grade){
  // mutator which sets the value of exam1 to exam1grade
 exam1=exam1grade;
  }

   void studentgrades::set_exam2(int exam2grade){
  exam2=exam2grade;
  }
  // mutator which sets the value of exam2 to exam2grade

   void studentgrades::set_final(int finalgrade){
  final=finalgrade;
  }

  void studentgrades::set_studentID(int id){
  studentID=id;
  }

  void studentgrades::computegrade()
  {
      overall=(.3*exam1)+(.3*exam2)+(.4*final);
      int  overall_rounded = overall;
      overall_rounded=overall_rounded/10;

    switch(overall_rounded)
    {
        case(10):
        grade='A';
        break;
        case (9):
            grade='A';
            break;
        case (8):
            grade='B';
            break;
        case (7):
            grade='C';
            break;
        case (6):
            grade='D';
            break;
        default:
            grade='F';
            break;
    }
  }

  double studentgrades::get_overall() const{

  return overall;
  }

char studentgrades::get_grade() const{

return grade;
}

int studentgrades::get_exam1() const{

return exam1;
}

int studentgrades::get_exam2() const{

return exam2;
}

int studentgrades::get_final() const{

return final;
}

int studentgrades::get_studentID()const{
return studentID;
}

void  studentgrades::output(ostream& outs)
{
//Precondition: If outs is a file output stream, then
//outs has already been connected to a file
//if (overallcomputed==true){
    if (outs==cout)
        outs<<"StudentID\t"<<"Exam 1\t"<<"Exam 2\t"<<"Final\t"<<"Overall\t"<<"Grade"<<endl;
        outs<<studentID<<"\t\t"<<exam1<<"\t"<<exam2<<"\t"<<final<<"\t"<<overall<<"\t"<<grade<<endl;
   // }
}

void studentgrades::reset_all()
{
    studentID=0;exam1=0;exam2=0;final=0;overall=0;
    overallcomputed=false;
    grade=' ';
}
