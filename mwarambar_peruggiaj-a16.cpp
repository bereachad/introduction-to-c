// Course: CSC 226 Introduction to Programming with C++
// Name: James Peruggia, Rutendo Mwaramba
// Lab 3: Graphing Dice Roll Outcomes 
/*
	Purpose: This program uses a Dice class to simulate a specified number of rolls of two dice and outputs
			a graphical repesentation of the frequency distribution of the resultant outcomes.
*/

/*







 */

#include<iostream>
#include<cmath>

using namespace std;

class RandGen
{
public:
	RandGen();                          // set seed for all instances
	int RandInt(int max = INT_MAX);     // returns int in [0..max)
	int RandInt(int low, int max);      // returns int in [low..max]
	double RandReal();                  // returns double in [0..1)
	double RandReal(double low, double max); // range [low..max]

	static void SetSeed(int seed);      // static (per class) seed set
private:
	static int ourInitialized;          // for 'per-class' initialization
};

class Dice
{
public:
	Dice(); 				// default constructor assumes a 6-sided die.
	int Roll();             // return the random roll of the die
	int NumSides() const;   // how many sides this die has

private:
	int mySides;            // # sides on die
};

void print( int roll_graph[], int size, int scale);
//Precondition: Dice rolls are complete and outcomes are recorded in the roll_graph array, array size and
//a calculated printing scale are given
//Postcondition: roll outcome frequecies are graphed
int max_check(int roll_graph[], int size, int DISPLAY_SIZE, int& scale);
//Precondition: Dice rolls are complete and outcomes are recorded in the roll_graph array, array size and printing 
//scale are given.
//Postcondition: Determines the maximum frequency and the appropriate graphing scale

int main()
{

	int total_roll(0), max(0), scale(0);
	long int roll_times;//accomodates a large number of specified rolls
	int roll_graph[13];//array declaration
	const int DISPLAY_SIZE = 75;

	//calls the default constructor to initialze two (6-sided) dice objects of the Dice class  
	Dice one;
	Dice two;

	cout<<"How many times would you like to roll?: ";
	cin>>roll_times;

	for (int i=0;i<13;i++) //sets all array values to 0
	{
		roll_graph[i]=0;
	}

	for(int x=1; x<=roll_times;x++)//rolls dice for the user-specified number of times 
	{
		total_roll=one.Roll()+two.Roll();// will roll two dice, adding the values of the rolls.
		int array_unit=total_roll;
		roll_graph[array_unit]++; //increments the value of the array variable corresponding to the total roll by 1
	}

	max = max_check(roll_graph, 13, DISPLAY_SIZE, scale);//checks for the maximum and calculates the graphing scale
    
    if(max<=DISPLAY_SIZE)
    scale = 1;//when the maximum frequency is less than or equal to 75 scale of asterrisk:frequency is 1:1
              //else the calculated scale is used   
	
    print(roll_graph, 13, scale);//prints the graph

	system("PAUSE");
	return 0;

}

int max_check(int roll_graph[], int size, int DISPLAY_SIZE, int& scale)
{
	int max(0); //sets the max to a value of zero
	for (int index=0;index<size;index++)//steps through all array variables
	{
		if (roll_graph[index+1]> roll_graph[index])//sets the max to the highest roll
		max=roll_graph[index+1];
	}

	scale = (max / DISPLAY_SIZE)+1;//computes the graphing scale - a pass-by-reference variable (value changes inside main)
	
	return max;
}

void print( int roll_graph[], int size, int scale)//prints out the frequency bar graph using the computed scale
{
	cout <<endl<<endl<<"Each asterick represents "<<scale<< " rolls"<<endl<<endl;
	for (int index = 2; index < size; index++)//loops through all the indexed variables to print the scaled asterisks
	{
        cout<<index<<": ";
       	int asterisk = roll_graph[index] / scale;// calculates the number of asterisks to be printed for each outcome of rolls
		for ( int count = 0; count < asterisk; count++)
		{
			cout<<"*";
		}
		cout<<endl;
		cout << "("<<roll_graph[index]<<")"<<endl;//prints the dice roll outcome below its asterisk bar
	}
}



int RandGen::ourInitialized = 0;

void RandGen::SetSeed(int seed)
// postcondition: system srand() used to initialize seed
//                once per program (This is a static function)
{
	if (0 == ourInitialized)
	{
		ourInitialized = 1;   // only call srand once
		srand(seed);          // randomize
	}
}


RandGen::RandGen()
// postcondition: system srand() used to initialize seed
//                once per program
{
	if (0 == ourInitialized)
	{
		ourInitialized = 1;          // only call srand once
		srand(unsigned(time(0)));    // randomize
	}
}

int RandGen::RandInt(int max)
// precondition: max > 0
// postcondition: returns int in [0..max)
{
	return int(RandReal() * max);
}

int RandGen::RandInt(int low, int max)
// precondition: low <= max
// postcondition: returns int in [low..max]
{
	return low + RandInt(max-low+1);
}

double RandGen::RandReal()
// postcondition: returns double in [0..1)
{
	return rand() / (double(RAND_MAX) + 1);
}

double RandGen::RandReal(double low, double high)
{
	double width = fabs(high-low);
	double thelow = low < high ? low : high;
	return RandReal()*width + thelow;
}

//---------------------------------------------------------------------
// Dice class functions begin here
//--------------------------------------------------------------------

Dice::Dice()//Default is to assume a six sided die.
// postcondition: all private fields initialized
{
	mySides = 6;
}


int Dice::Roll()
// postcondition: number of rolls updated
//                random 'die' roll returned
{
	RandGen gen;    // random number generator
	return gen.RandInt(1,mySides);        // in range [1..mySides]
}

int Dice::NumSides() const
// postcondition: return # of sides of die
{
	return mySides;
}





