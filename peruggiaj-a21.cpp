// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 21: <This program deals 2 hands of 25 random cards(0-9) and
                                // will play war with one another.  Winner is displyed at the end
/*
    Purpose: <This program makes use of the vector class.  Simple vector functions like
                    //pop_back() and back() are used.>
*/

#include<iostream>
#include<cstdlib>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

class War {
  public:
    War(); // constructor

    bool any_turns(); // returns true if there are moves left, false if there is a winner.

  private:
    vector <int> p1;
    vector<int>p2;
    vector <int>p1store;
    vector<int>p2store;
    vector<int>loot;

    void compare(); // will compare p1 to p2
    void declare_War(); // will take one card from p1 and p2 and put it in the loot;

    int get_p1(); // will get a card from p1 and put it in loot.  Also checks to see if p1 deck is empty, if it is, will take from store.
    int get_p2(); // will get a card from p2 and put it in loot  Also checks to see if p2 deck is empty.  If it is, will take from store.

    void movestorep1(); // move p1 store to p1 hand;
    void movestorep2(); // move p2 store to p2 hand;
};

int main(){

    War newGame;

    while (newGame.any_turns()==true);  // while loop will occur till it is false
    system("PAUSE");
    return 0;
}

War::War()
{
    srand(time(0));
    rand();
    //create player deck
    for (int x=0; x < 25; x++)
        p1.push_back(static_cast<int>(10.0 * (rand() / (RAND_MAX + 1.0))));
    // create other deck
    for (int y=0; y < 25; y++)
        p2.push_back(static_cast<int>(10.0 * (rand() / (RAND_MAX + 1.0))));
}

bool War::any_turns(){

    if ( p1store.empty() && p1.empty() ){  // no cards in p1 and p1 store
        cout<<"\nPlayer 2 Wins!!"<<endl;
        return false;
    }
    else if ( p2.empty() && p2store.empty() ){  // no cards in p2 or p2 store
        cout<<"\nPlayer 1 Wins!!"<<endl;
        return false;
    }
    else{
        compare();
        return true;
    }
}

void War::compare(){
    int p1card=get_p1();
    int p2card=get_p2();
    cout<<"Player 1 : "<<p1card<<"\tPlayer 2: "<<p2card;
    if (p1card > p2card){
            cout<<"\t Player 1's Hand"<<endl;
             while(!loot.empty()){
                p1store.push_back(loot.back());
                loot.pop_back();
           }
    }
    else if (p2card>p1card){  // player 2 is more then player 1
            cout<<"\t Player 2's Hand"<<endl;
           while(!loot.empty()){
                p2store.push_back(loot.back());
                loot.pop_back();
           }
    }
    else{ //(p1card==p2card) // war!!
        cout <<"\tWAR!!"<<endl;
        declare_War();
    }
}

int War::get_p1(){

    if (p1.empty()){
        movestorep1();  // move the store into p1
        p1store.clear();  // clear store after the move
    }
    int number = p1.back();
    loot.push_back(number);
    p1.pop_back();
    return number;
}

int War::get_p2(){
    if (p2.empty()){
        movestorep2();  // move the store into p2
        p2store.clear();  // clear p2 store
    }

    int number =p2.back();  // sets number to the lasts one in the vertex
    loot.push_back(number);  // pushes that into loot
    p2.pop_back();  // deletes it from vector
    return number;
}

void War::movestorep1(){
    while (!p1store.empty()){  // while store isn't empty
        p1.push_back(p1store.back());  // push last store into p1
        p1store.pop_back();  // delete the last store
    }

}

void War::movestorep2(){
    while (!p2store.empty()){// while store isn't empty
        p2.push_back(p2store.back()); // push last store into p2
        p2store.pop_back(); // delete the last store
    }
}

void War::declare_War(){
    if (p1.size() ==  0)
        movestorep1();
    if(p2.size() == 0)
        movestorep2();
    loot.push_back(p1.back());  // put p1 card in loot
    loot.push_back(p2.back());  // put p2 card in loot
    p1.pop_back();  // remove p1 card from deck
    p2.pop_back();  // remove p2 card from deck
}
