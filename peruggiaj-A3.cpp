// Course: CSC 226 Introduction to Programming with C++ 
// Name: Chad Peruggia
// Assignment 3: Temperature Convertor as well as Currencey changer
/* Purpose: 
	<This program can be used by someone traveling to Britian.  It will display 
	a list of common temperatures as well as the corresponding Fahrenheit temp.
	After this the user is allowed to input a conversion rate for Dollars to 
	Pounds.  They are then given a list of common denominations in pounds with how
	many American Dollars that is>
*/


#include <iostream>

using namespace std;

int main()
{
    
    double Far, cel(-1.0), conversion_rate, dollars, pounds(5);
    // Table for temperature header
    cout<<"Temperature Conversion Table\n";
    cout<<"------------------------------\n";
    cout<<"Celcius"<<"\t"<<"\t"<<"Fahrenheit\n";
    cout<<"--------"<<"||"<<"-------------------\n";
    
    /*Loop while celcius is equal to or less then 30
    it does the math, prints the information, then adds 1
    to the celcius value */
    do{
        Far=cel*(9.0/5)+32.0;
        cout<<cel<<"\t"<<"||"<<"\t"<<Far<<"\n";
        cel++;
        }while(cel<=30);
    
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2); 
    
    //Prompt for user input and header for Money Conversion
    cout<<"\nCurrency Conversion\n";
    cout<<"Please enter the amount of USD for 1 GBP:";
    cin>>conversion_rate;
    cout<<"\n"<<"\nPounds"<<"\t"<<"\t"<<"\t"<<"Dollars\n";
    cout<<"----------------||----------------\n";
    
    do{
    // Placed a loop in here to make the table formatting nicer, same function,
    // just one less \t to allow proper spacing when pounds >= 100.
       dollars=pounds*(conversion_rate);
           if(pounds>=100)
           {
               cout<<(char)156<<" "<<pounds<<"\t"<<"||"<<"\t$"<<dollars<<"\n";
           }
           else
           {
               cout<<(char)156<<" "<<pounds<<"\t"<<"\t"<<"||"<<"\t$"<<dollars<<"\n";
           }
       pounds=pounds+5;
           
       }while(pounds<=200);
       
    cout<<"Thank You\n";
    system("Pause");
    return 0;
}
