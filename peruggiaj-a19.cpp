// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 19: <This program takes lists of numbers and sorts them in different ways.>
/*
    Purpose: <This program uses two different methods of sorting, either selection sort or insertion sort.
    The user will select which method they prefer, and it will sort it accordingly.>

    I.  It seems that selection sort does a ton of comparissons, but not many swaps.  The insertion sort seems to do
    an equal amount of both.

    II.  The selection sort showed a low number of swaps but a huge number of comparisions, but the insertion sort showed a low number of
        swaps as well as comparisons.  This was not what I expected.

    III.  When using descencing methods on things that are nearly sorted, the results are a bit more of what I expected.
    The selection sort does a low number of swaps with a high number of comps, but the insertion does a high number of both.

*/

#include<iostream>
#include <fstream>
#include<cstdlib>

using namespace std;

const int MAX_NUMBER = 250;

class ArraySorter{

public:

    ArraySorter();  // constructor will ask for input name and call inputArray Function

   void inputArray(istream& input);  // called in the constructor, will fill the array.  Added to increase
    //readability.

    void copyArray();
    //precondition: storedArray was made, and now will copy it to Working array

    void selectAscending();
    //precondition: workingArray was made and user selected this option
    //postcondition: will sort from smalles to largest using this method, when done will print and
    //reset working array
    void selectDescending();
    //precondition: workingArray was made and user selected this option
    //postcondition: will sort from largest to smallest using this method, when done will print and
    //reset working array
    void insertionAscending ();
    //precondition: workingArray was made and user selected this option
    //postcondition: will sort from smalles to largest using this method, when done will print and
    //reset working array

    void insertionDescending();
    //precondition: workingArray was made and user selected this option
    //postcondition: will sort from largest to smallest using this method, when done will print and
    //reset working array

    void printWorking(ostream& oput);
    //precondition:  the workingArray was made
    //postcondition : will print the array

    void swap_values(int& first, int& second);
//Interchanges the values of first and second.  Adds one to swap counter if they aren't the same

    int index_of_smallest(int start_index);
//Precondition: 0 <= start_index < numberUsed. References array elements have values.
//Returns the index i such that array_to_sort[i] is the smallest of the values
//array_to_sort[start_index], array_to_sort[start_index + 1], ..., array_to_sort[numberUsed - 1].

    int index_of_largest(int start_index);
//Precondition: 0 <= start_index < numberUsed. References array elements have values.
//Returns the index i such that array_to_sort[i] is the largest of the values
//array_to_sort[start_index], array_to_sort[start_index + 1], ..., array_to_sort[numberUsed - 1].

    int numSwaps();// return the num of Swaps to print
    int numComp();  // returns the num of Comps to print

private:
    int storedArray[MAX_NUMBER];
    int numberUsed;
    int workingArray[MAX_NUMBER];
    int Swaps;
    int Comps;
    bool workedon; // boolean to see if work has been done, used in print statement


};

void Menu();


int main()
{
    int swaps, comps;
    int choice;
    char outputfilename[20];
    ofstream output;
    ArraySorter creation; // created object of ArraySorter
     // displays menu
    do{
    Menu(); //displays menu
    cin>>choice;
        switch (choice) // takes the user input and acts accordingly
        {
            case (1):
                creation.copyArray(); // used to clear all memeber variables and recopy before worked on
                creation.selectAscending();
                swaps= creation.numSwaps();  // return the number of swaps
                comps=creation.numComp();  // return the number of comps
                cout<<"There were "<<swaps<<" swaps and "<<comps<<" comparisions\n";
                break;
            case(2):
                creation.copyArray(); // used to clear all memeber variables and recopy before worked on
                creation.selectDescending();
                swaps= creation.numSwaps();  // return the number of swaps
                comps=creation.numComp();  // return the number of comps
                cout<<"There were "<<swaps<<" swaps and "<<comps<<" comparisions\n";
                break;
            case(3):
                creation.copyArray(); // used to clear all memeber variables and recopy before worked on
                creation.insertionAscending();
                swaps= creation.numSwaps();  // return the number of swaps
                comps=creation.numComp();  // return the number of comps
                cout<<"There were "<<swaps<<" swaps and "<<comps<<" comparisions\n";
                break;
            case(4):
                creation.copyArray(); // used to clear all memeber variables and recopy before worked on
                creation.insertionDescending();
                swaps= creation.numSwaps();  // return the number of swaps
                comps=creation.numComp();  // return the number of comps
                cout<<"There were "<<swaps<<" swaps and "<<comps<<" comparisions\n";
                break;
            case (5):
                cout<<endl<<"What is the output file name? ";
                cin>>outputfilename;
                output.open(outputfilename);
                creation.printWorking(output);
                creation.copyArray(); // will recopy working array to its original state
                break;
            case (6):
                creation.printWorking(cout);
                creation.copyArray(); // will recopy working array to its original state
                break;
            case(7):
                return (1);
                break;
        }
    }while((choice > 0) && (choice < 6));

}


void Menu()
{
    cout <<endl<< "1. Selection sort Ascending Order\n"<< "2. Selection sort Desceding Order\n"
                        << "3. Insertion sort Ascending Order\n"<< "4. Insertion sort Descending Order\n"
                        <<"5. Print Sorted to a File\n"<<"6. Print to Screen\n"<<"7. Quit\n";
}

/*  CLASS DEFINTIONS */

 ArraySorter::ArraySorter(){
    numberUsed=0, Swaps = 0, Comps = 0;
    char input_name[20];
    workedon= false;
    ifstream input;

    cout<<"What is the name of the file to be sorted?";
    cin >> input_name;
    input.open(input_name);
    while(input.fail())
    {  // if file can't open will tell the user and will ask for a new one
        cout<<"Open "<<input_name<< " failed. Please try again\n"<<endl;
        input.close();
        input.clear();
        cin>>input_name;
        input.open(input_name);
    }
    inputArray(input);
}

void ArraySorter::selectAscending(){
    workedon=true; // work was done
    int indexOfNextSmallest;
    for (int index = 0; index < numberUsed - 1; index++)
    {//Place the correct value in arrayToSort[index]:
        indexOfNextSmallest = index_of_smallest(index);
        swap_values(workingArray[index], workingArray[indexOfNextSmallest]);
        //arrayToSort[0] <= arrayToSort[1] <=...<= arrayToSort[index] are the smallest of the original array
        //elements. The rest of the elements are in the remaining positions.
    }
}

int ArraySorter::index_of_smallest(int startIndex)  // finds smallest number , used in selection sort
{
	int minIndex = startIndex;
    for (int index = startIndex + 1; index < numberUsed; index++)
       {
            Comps++; // adds one to comparrisons
        if ( workingArray[index] < workingArray[minIndex] )
        {

            minIndex = index;
            //min is the smallest of arrayToSort[startIndex] through arrayToSort[index]
        }
       }
    return minIndex;
}

void ArraySorter::selectDescending(){  // selection sort descending
    workedon=true; // work was done
  int indexOfNextLargest;
    for (int index = 0; index < numberUsed - 1; index++)
    {//Place the correct value in arrayToSort[index]:
        indexOfNextLargest = index_of_largest(index);
        swap_values(workingArray[index], workingArray[indexOfNextLargest]);
        //arrayToSort[0] >= arrayToSort[1] >=...>= arrayToSort[index] are the largest of the original array
        //elements. The rest of the elements are in the remaining positions.
    }
}

int ArraySorter::index_of_largest(int startIndex)  // finds largest number used in selection sort
{
	int maxIndex = startIndex;
    for (int index = startIndex + 1; index < numberUsed; index++){
            Comps++;  // adds one to comparrisonse
        if ( workingArray[index] > workingArray[maxIndex] )
        {
            maxIndex = index;
            //min is the smallest of arrayToSort[startIndex] through arrayToSort[index]
        }
    }
    return maxIndex;
}

void ArraySorter::insertionAscending()  //insertion Ascending function
{
    workedon=true; // work was done
    for (int index =1; index < numberUsed; index++)
    {
        int newSpot=index;
        Comps++; // add one because it is doing a check to start teh loop;
        while( newSpot > 0 && workingArray[newSpot-1] > workingArray[newSpot] ) {
              swap_values(workingArray[newSpot-1], workingArray[newSpot]);
              newSpot --;	// decrement newSpot to move the insertion downwards
              Comps++;
        }
    }
}

void ArraySorter::insertionDescending()  // insertion Descending function
{
    for (int index =1; index < numberUsed; index++)
    {
        workedon=true; // work was done
        int newSpot=index;
        Comps++; // add one because it is doing a check to start teh loop;
        while( newSpot > 0 && workingArray[newSpot-1] < workingArray[newSpot] ) {
              swap_values(workingArray[newSpot-1], workingArray[newSpot]);
              newSpot --;	// decrement newSpot to move the insertion downwards
              Comps++;
        }
    }
}

void ArraySorter::swap_values(int& first, int& second)  // swap function called when a swap is needed
{
    if (first != second)
        Swaps++;  // adds one to swap counter if the numbers swapped were not the same
   int temp = first;
   first = second;
   second = temp;
}

void ArraySorter::inputArray(istream& input)  // takes the numbers from the file and puts them in the array
{
    int number;
    while (input>> number)  // while it is still pulling in a number, it will place in array
    {
        storedArray[numberUsed]=number;
        numberUsed ++;
    }
    copyArray(); // when done filling the array, it will copy it to workingArray
}

void ArraySorter::copyArray()  // copys the stored array into a working array
{
    Swaps = 0; Comps= 0;workedon=false; // reseting all class private member variables
    for (int x = 0; x< numberUsed; x++)
        workingArray[x]=storedArray[x];
}

void ArraySorter::printWorking(ostream& oput)  // print function, will print to desired ostream
{

    if (workedon==true){  // if something was done we want to print
    for (int x = 0; x < numberUsed; x++)
        oput <<storedArray[x]<<"\t" <<workingArray[x]<<endl;
    oput <<Swaps<<"Swaps" <<endl<<Comps<<"Comps"<<endl;
    }
    else
        oput<<"No work was done on the array.  We won't print it";
}

int ArraySorter::numSwaps()
{
    return Swaps;
}

int ArraySorter::numComp()
{
    return Comps;
}
