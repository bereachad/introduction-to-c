#include<iostream>
#include<cstdlib>
#include <fstream>

using namespace std;

const int MAX_ROWS=6;
const int MAX_COLS= 8;

class Exam2Array{

    public:
        Exam2Array(char filename[30], int rows, int cols); // constructor

        void print(ostream& output);

        void flip_rows();


    private:
        int array[MAX_ROWS][MAX_COLS];
        int rows_used;
        int cols_used;


};


int main()
{
    char filename[30];
    int rows, cols;
    cout << "Please enter the file in which you want to place into an array:\n";
    cin>>filename;
    cout<<"How many rows are in this array?";
    cin>>rows;
    cout<<"How many columns?";
    cin>>cols;

    Exam2Array test(filename, rows, cols);
    test.print(cout);
    cout <<endl<<endl;
    test.flip_rows();
    test.print(cout);

}

Exam2Array::Exam2Array(char filename[30], int rows, int cols)
{
    rows_used = rows; cols_used = cols;
    if ((rows <MAX_ROWS) && (cols < MAX_COLS))
    {
        ifstream input;
        input.open(filename);

        if (input.fail()==false)
        {
            for (int r = 0 ; r< rows; r++)  // row for loop, will move down once the coloumn if full
                for (int c = 0 ; c < cols; c++)
                {
                    input>>array[r][c];
                }
        }
        else
            cout << "Error opening file!"<<endl;
    }
}

void Exam2Array::print(ostream& output)
{
    for (int r= 0; r < rows_used; r++)
    {
        for (int c= 0; c<cols_used; c++)
            output<<array[r][c]<<" ";
        output<<endl;
    }
}

void Exam2Array::flip_rows()
{
    int temp;
    int x =0;
    for (int r= (rows_used-1);r >= (rows_used /2); r--)
    {
        for (int c=0; c < cols_used; c++)
        {
            temp=array[r][c];  // sets a temp value
            array[r][c]= array[(x )][c]; // switches the value you are on with the value of the max row - number of rows you have done
            array[(x )][c] =temp; // sets the max row to the value of the original
        }
            x++;
    }
}
