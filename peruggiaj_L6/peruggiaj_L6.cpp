// Course: CSC 226 Introduction to Programming with C++ 
// Name: Chad Peruggia
// Lab 6: Enumeration Types and the Switch Statement
/* 
    Purpose: <This program makes a deck of cards using a card object.  It also uses enum types.
             The Program will also shuffel and deal out the cards to four different players.>
*/

#include<iostream>
#include<string>
#include<cstdlib>
#include "lab6.h"

using namespace std;

int main()
{
    bool dealt_out=false; 
    int index=0, choice(0), cards_hand(0);
    SUIT fillsuit;
    RANK fillrank;
    Card Deck[52]; // 52 card objects
    Card player_hands[4][13]; // multi dimensional array, 4 X 13, 4 players, max of 13 cards per

    for (int s=CLUBS; s<=SPADES; s++) // for loop, while there are still suits to be made
        for (int v=ACE; v<= KING; v++) // pass out cards 2 - Ace
        {
            fillsuit=static_cast<SUIT>(s); // get the enum back
            fillrank=static_cast<RANK>(v); // get the enum back
            Deck[index].set_type(fillsuit); // fill set card with suit
            Deck[index].set_rank(fillrank);// fill set card with rank
            index++;
        }

  while(choice < 6){ // while loop for menu and switch statement
    home_menu();
    cout<<"\nChoice: ";
    cin>>choice;
    switch (choice){
        case(1): // display deck
            for (int i=0; i < 52; i++)
                cout<<Deck[i]<<endl;
            break;
        case(2): // shuffle deck
            shuffle_deck(Deck);
            break;
        case (3):
            make_card();
            break;
        case(4): // deal cards out
            dealt_out=true;
            cout<<"How many cards per hand? ";
            cin>>cards_hand;
            deal_cards(Deck, player_hands, cards_hand); // call to deal function
            break;
        case(5):
            if (dealt_out==true){ // if cards are out, they can print hands
            print_hands(player_hands, cards_hand);
            break;}
            else{// they will see option 4 as exit
                exit(1);
                break;
            }
        case(6): // exit
            exit(1);
            break;
        default:
            break;
    }
  }

}
