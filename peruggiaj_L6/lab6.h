#include<iostream>
#include<cstdlib>
#include <ctime>
#include<string>

using namespace std;

#ifndef LAB6_H
#define LAB6_H

enum SUIT{CLUBS=1, DIAMONDS, HEARTS, SPADES};
enum RANK{ACE=1,TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};


class Card{
    public:
        Card(); // constructor
        Card(SUIT suit, RANK rank);  // constructor with parameters
        const char get_type();
        //precondition: Card has been made
        //post condition: Will return the type of card in a symbol form
        const string get_rank();
        //precondition: Card has been made
        //postcondition: Will return the Rank of the card as a string
        void set_rank(RANK rank);
        //precondition: Object has been made
        //post condition: will set the rank of the card
        void set_type(SUIT suit);
        //precondition: Object has been made
        //postcondition: Will set the suit of the card
        friend ostream& operator << (ostream& outs, Card& out_object);

    private:
        SUIT suit_of_card;
        RANK rank_of_card;
};

void shuffle_deck(Card Deck[]);
//precondition: An array of Card objects has been made
//postconiditon: will shuffle the deck randomly

void make_card();

void deal_cards(const Card Deck[], Card players[][13], int to_deal);
// precondition: None
//postcondition: Will deal the cards to 4 people, user will specify how many cards per hand.
void print_hands(Card players[][13], int cards_hand);
//precondition: The cards have been delt.
//postcondition: Will print the hands to the screen
void home_menu();
//function to show user menu

void suit_menu(); // menu to show suit options
void rank_menu(); // menu to show rank options



#endif
