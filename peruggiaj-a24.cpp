// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 24: <This program uses recursion to return the nth number of the fibonacci sequence>


#include<iostream>

using namespace std;

int fibonacci (int input);
//precondition: User has input a number
// postcondition: will return the value of the fibonacci sequence using recursion

int main()
{
    int user_input;
    cout <<"WElcome to the FIbonacci number program!\n"<<endl;
    cout<<"\nPlease enter a number and we will return that Fibonacci Number:";
    cin>>user_input;
    cout<<"The number returned is : "<<fibonacci(user_input)<<endl;
}

int fibonacci (int input){
    if (input ==0 || input == 1)  // if the input is down to 0 or 1
    {
        return 1;
    }
    else
        return ( fibonacci(input-1) + fibonacci(input-2)) ; // will add the previous two numbers
}
