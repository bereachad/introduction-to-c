// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia  Rohan Issac
// Assignment #2: Using C++ Types and Modulus
/* Our program allows the user to input Dollars then Cents and it will
tell you the bill breakdown of how to give the change*/
/* We finished the program pretty quick.  Rohan was the driver before
I (Chad) had a chance to do so.  We developed the algorithim together.  It worked
the first time we ran the program so no further debugging how to be done*/


#include <iostream>

using namespace std;

int main()
{
    int dollars, cents;
    cout << "Enter the money in dollar and cents: ";
    cin >> dollars >> cents;

    cout << "\nYou entered $" << dollars << "." << cents << endl;

    //divines the dollars by set amount of bills, then takes the remainder and divides
    //it and resets the rem value each time
    int twenty, ten, five, ones, quarters, dime, nickels, pennies, rem;
    twenty = dollars/20;
    rem = dollars%20;
    ten = rem/10;
    rem = rem%10;
    five = rem/5;
    rem = rem%5;
    ones = rem;

    //divides the change amount by total of each coin, then takes the remainder value
    //and divides it till there is no change left.  Rem value is reset each time

    quarters = cents/25;
    rem = cents%25;
    dime = rem/10;
    rem = rem%10;
    nickels = rem/5;
    rem = rem%5;
    pennies = rem;

    cout << "\nTwenties: " << twenty << "\nTen: " << ten << "\nFive: " << five << "\nOnes: " << ones
    << "\nQuarters: " << quarters << "\nDimes: " << dime << "\nNickels: " << nickels << "\nPennies: " << pennies << endl;

//    system("PAUSE");
    return 0;
}
