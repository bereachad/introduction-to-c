#include<iostream.h>
#include<windows.h>
#include<cstdlib>
#include<iomanip.h>
#include <conio.h>
#include"connectfour.h"

#define YES 1
#define NO 0
#define PLAYER1 1
#define PLAYER2 2
#define ENTER 13
#define SPACE 32
#define BACK 8
#define ESC 0x1b
#define LEFT 0x4b
#define RIGHT 0x4d



Game::Game(){
 // constructor left blank on purpose
}

void Game::run(){
    cout<<"Player one enter your name:"<<endl; // gets players names and puts them into the array
	cin.getline(playerone, size);
	system("cls");
	cout<<"Player two enter your name:"<<endl;
	cin.getline(playertwo, size);
	system("cls");
	while (1) // loop to see if they are playing
	{
		system("cls");
		quit=false;  // set private boolean varible
		arrowpos=0;  // set position variable
		resetboard(board);  // call to reset game
		resetbool(blueboolboard, redboolboard, cboard);  // reset all boollean values
		cout<<"PRESS ESCAPE AT ANY TIME TO QUIT"<<endl<<endl;
		system("pause");
		system("cls");
		while (quit==false)
		{
			if(win(redboolboard, blueboolboard)!=0)  // calls function to see winner, and then if it is not false
			{
				quit=true; // there was somoene that won the game
			}
			if (quit==false) // if they are still playing
			{
				number=getboard(board, cboard, blueboolboard, redboolboard, playerone, size, PLAYER1); // gets the board for player 1, passing in teh parameters needed.
				stop=false;
			}
			else
			{
				number=8;  // escape key
			}
			if (number==8) // if escape key was pressed
			{
				quit=true; // end the game
			}
			else // they are playing the game!
			{
				system("cls");
				if (cboard[5][number]==false)  // if not at the end of the board
				{
					falling(board, redboolboard, blueboolboard, cboard, number, playerone, size, PLAYER1);
					cboard[5][number]=true;
					system("cls");
					redboolboard[5][number]=true;
				}
				else if(cboard[0][number]!=true)
				{
					for (x=4; x>=0; x--)
					{
						if (stop==false)
						{
							if (cboard[x][number]!=true)
							{
								falling(board, redboolboard, blueboolboard, cboard, number, playerone, size, PLAYER1);
								redboolboard[x][number]=true;
								system("cls");
								cboard[x][number]=true;
								stop=true;
							}
						}
					}
				}
			}
			//RED'S TURN
			stop=false;
			if (win(redboolboard, blueboolboard)!=0)
			{
				quit=true;
			}
			else if(quit!=true)
			{
				number=getboard(board, cboard, blueboolboard, redboolboard, playertwo, size, PLAYER2);
				if (number==8 || win(redboolboard, blueboolboard)!=0)
				{
					quit=true;
				}
				if (quit==false)
				{
					if (cboard[5][number]==false)
					{
						falling(board, redboolboard, blueboolboard, cboard, number, playertwo, size, PLAYER2);
						cboard[5][number]=true;
						system("cls");
						blueboolboard[5][number]=true;
					}
					else
					{
						for (x=4; x>=0; x--)
						{
							if (stop==false)
							{
								if (cboard[x][number]!=true)
								{
									falling(board, redboolboard, blueboolboard, cboard,number, playertwo, size, PLAYER2);
									blueboolboard[x][number]=true;
									cboard[x][number]=true;
									system("cls");
									stop=true;
								}
							}
						}
					}
				}
			}
		}
		system("cls");
		winningboard(board, blueboolboard, redboolboard);
		if (win(redboolboard, blueboolboard)==1)
		{
			for (d=0; d<size, playerone[d]!='\0'; d++)
			{
				//all caps
				if (playerone[d]<=122 && playerone[d]>=97)
					playerone[d]-=32;
				cout<<playerone[d];
			}
			cout<<" WINS!!!";
		}
		else if (win(redboolboard, blueboolboard)==2)
		{
			for (d=0; d<size, playertwo[d]!='\0'; d++)
			{
				//all caps
				if (playertwo[d]<=122 && playertwo[d]>=97)
					playertwo[d]-=32;
				cout<<playertwo[d];
			}
			cout<<" WINS!!!";
		}
		else
		{
			cout<<"it's a tie";
		}
		cout<<endl;
		cout<<"PLAY AGAIN?\n1=NO\n2=YES"<<endl;
		cin>>yesorno;
		yesorno--;
		if (yesorno==NO)
		{
			exit(1);
		}
}
	//return 0;
}

void Game::setcolor (int background, int letter)
{
	int color=background*16+letter;
	cout<<flush;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}
void Game:: viewchar()
{
	char letter;
	system("cls");
	for (int x=0; x<=255; x++)
	{
		letter=x;
		cout<<x<<": "<<letter<<"\t";
		if(x%6==0)
		{
			cout<<endl;
		}
	}
	system("pause");
}

void set_color (int background, int letter)
{
	int color=background*16+letter;
	cout<<flush;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}
void Game::resetboard(char board[][7])
{
	for (int row=0; row<6; row++)
	{
		for (int collumn=0; collumn<7; collumn++)
		{
			board[row][collumn]=15;
		}
	}
}
int Game::getboard(char board [][7], bool cboard[][7], bool blueboolboard[][7], bool redboolboard[][7], char name[], const int num, const int who)
{
	char letter;
	char arrow[7]={' ',' ',' ',' ',' ',' ',' '};
	int arrowpos;
	int t;
	displayboard(board, blueboolboard, redboolboard, name, num, who);

	int r=0;
	for (r; r<7; r++)
	{
		if (cboard[0][r]==false)
		{
			arrow[r]=15;
			arrowpos=r;
			break;
		}
		if (r==6 && cboard[0][6]==true)
		{
			return 8;
		}
	}
	while (t!=13 && t!=27)
	{
		t=getch();
		system("cls");
		for (int a=0; a<20, name[a]+='\0'; a++)
		{
			cout<<name[a];
		}
		cout<<endl<<endl<<endl;
		if (t==LEFT)
		{
			if (arrowpos!=0)
			{
				if (cboard[0][arrowpos-1]==false)
				{
					arrow[arrowpos]=' ';
					arrowpos--;
					arrow[arrowpos]=15;
				}
				else
				{
					for (int i=arrowpos-1; i>=0; i--)
					{
						if (cboard[0][i]==false)
						{
							arrow[i]=15;
							arrow[arrowpos]=' ';
							arrowpos=i;
							break;
						}
					}
				}

			}
		}
		else if (t==RIGHT)
		{
			if (arrowpos!=6)
			{
				if (cboard[0][arrowpos+1]==false)
				{
					arrow[arrowpos]=' ';
					arrowpos++;
					arrow[arrowpos]=15;
				}
				else
				{
					for (int i=arrowpos+1; i<7; i++)
					{
						if (cboard[0][i]==false)
						{
							arrow[i]=15;
							arrow[arrowpos]=' ';
							arrowpos=i;
							break;
						}
					}
				}
			}
		}
		cout<<"    ";
		setcolor(BLACK, LIME);
		for (int w=0; w<7; w++)
		{
			if (who==PLAYER1)
			{
				setcolor(BLACK, RED);
			}
			else
			{
				setcolor(BLACK, LIGHT_BLUE);
			}
			cout<<arrow[w]<<' ';
		}
		setcolor(BLACK, WHITE);
		cout<<endl;
		for (int x=0; x<6; x++)
		{
			cout<<setw(5);
			for (int y=0; y<7; y++)
			{
				if (blueboolboard[x][y])
				{
					setcolor(BLACK, LIGHT_BLUE);
					cout<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
				else if (redboolboard[x][y])
				{
					setcolor(BLACK, RED);
					cout<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
				else
				{
					setcolor(BLACK, YELLOW);
					cout<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
			}
			cout<<endl;
		}
		setcolor(BLACK, BRIGHT_BLUE);
		letter=219;
		cout<<"    "<<letter<<"           "<<letter<<endl;
		letter=30;
		cout<<endl;
		setcolor(BLACK, WHITE);
	}
	if (t!=27)
		return arrowpos;
	else
		return 8;
}
void Game::resetbool (bool blueboolboard[][7], bool redboolboard[][7], bool cboard[][7])
{
	for (int x=0; x<6; x++)
	{
		for (int y=0; y<7; y++)
		{
			blueboolboard[x][y]=false;
			redboolboard[x][y]=false;
			cboard[x][y]=false;
		}
	}
}
void Game::falling(char board[][7], bool redboolboard[][7], bool blueboolboard[][7], bool cboard[][7], int num, char name[], int number, const int who)
{
	char letter;
	int stop;
	if (cboard[5][num]==false)
		stop=5;
	else
	{
		for (int q=4; q>=0; q--)
		{
			if (cboard[q][num]==false)
			{
				stop=q;
				break;
			}
		}
	}
	for (int r=0; r<=stop; r++)
	{
		system("cls");
		for (int a=0; a<20, name[a]!='\0'; a++)
		{
			cout<<name[a];
		}
		cerr<<endl<<endl<<endl<<endl;
		if (who==PLAYER1)
		{
			redboolboard[r][num]=true;
		}
		if (who==PLAYER2)
		{
			blueboolboard[r][num]=true;
		}
		for (int x=0; x<6; x++)
		{
			cerr<<setw(5);
			for (int y=0; y<7; y++)
			{
				if (blueboolboard[x][y])
				{
					setcolor(BLACK, LIGHT_BLUE);
					cerr<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
				else if (redboolboard[x][y])
				{
					setcolor(BLACK, RED);
					cerr<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
				else
				{
					setcolor(BLACK, YELLOW);
					cerr<<board[x][y]<<' ';
					setcolor(BLACK, WHITE);
				}
			}
			cerr<<endl;
		}
		if (who==PLAYER1)
		{
			redboolboard[r][num]=false;
		}
		if (who==PLAYER2)
		{
			blueboolboard[r][num]=false;
		}
		setcolor(BLACK, BRIGHT_BLUE);
		letter=219; // makes a box to print in blue (bottom of board)
		cout<<"    "<<letter<<"           "<<letter<<endl;
		letter=30; // makes symbol
		cout<<endl;
		setcolor(BLACK, WHITE);
		cout<<endl;
		Sleep(100); // timer to pause
	}
}
void Game::displayboard(char board [][7], bool blueboolboard[][7], bool redboolboard[][7], char name[], const int size, const int who)
{
	char letter=15;
	char arrow[7]={' ',' ',' ',' ',' ',' ',' '};
	for (int a=0; a<20, name[a]!='\0'; a++)
		cout<<name[a];
	cout<<endl;
	cout<<endl<<endl;
	for (int i=0; i<7; i++)
	{
		if (redboolboard[0][i]==false && blueboolboard[0][i]==false)
		{
			arrow[i]=15;
			break;
		}
	}
	if (who==PLAYER1)
	{
		setcolor(BLACK, RED);
	}
	else if (who==PLAYER2)
	{
		setcolor(BLACK, LIGHT_BLUE);
	}
	cout<<"    ";
	for (int u=0; u<7; u++)
	{
		cout<<arrow[u]<<' ';
	}
	cout<<endl;
	setcolor(BLACK, WHITE);
	for (int x=0; x<6; x++)
	{
		cout<<setw(5);
		for (int y=0; y<7; y++)
		{
			if (blueboolboard[x][y])
			{
				setcolor(BLACK, LIGHT_BLUE);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
			else if (redboolboard[x][y])
			{
				setcolor(BLACK, RED);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
			else
			{
				setcolor(BLACK, YELLOW);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
		}
		cout<<endl;
	}
	setcolor(BLACK, BRIGHT_BLUE);
	letter=219;
	cout<<"    "<<letter<<"           "<<letter<<endl;
	letter=30;
	cout<<endl;
	setcolor(BLACK, WHITE);
}
int Game::win(bool redboard [][7], bool blueboard [][7])
{
	//int win1, win2, win3, win4;
	//across loops
	for (int c =0; c < 6; c ++)
        for (int r=0; r <4; r++){
            if (redboard[c][r]==true && redboard[c][r+1]==true &&redboard[c][r+2]==true &&redboard[c][r+3]==true)
            {
                return PLAYER1;
            }
            if (blueboard[c][r]==true && blueboard[c][r+1]==true &&blueboard[c][r+2]==true &&blueboard[c][r+3]==true)
            {
                return PLAYER2;
            }
        }

	//down loops
	for (int r =0; r < 7; r ++)
        for (int c=0; c <3; c++){
            if (redboard[c][r]==true && redboard[c+1][r]==true &&redboard[c+2][r]==true &&redboard[c+3][r]==true)
            {
                return PLAYER1;
            }
            if (blueboard[c][r]==true && blueboard[c+1][r]==true &&blueboard[c+2][r]==true &&blueboard[c+3][r]==true)
            {
                return PLAYER2;
            }
        }

	//diagonal bottomleft to top right
	for (int r=3; r < 6; r ++)
        for (int c =0; c < 4; c++){
            if (redboard[r][c]==true && redboard[r-1][c+1]==true && redboard[r-2][c+2]==true && redboard[r-3][c+3]==true)
            {
                return PLAYER1;
            }
            if (blueboard[r][c]==true && blueboard[r-1][c+1]==true && blueboard[r-2][c+2]==true && blueboard[r-3][c+3]==true)
            {
                return PLAYER2;
            }
        }

	//diagonal top left to bottom right
	for (int r=0; r < 4; r ++)
        for (int c =0; c < 4; c++){
            if (redboard[r][c]==true && redboard[r+1][c+1]==true && redboard[r+2][c+2]==true && redboard[r+3][c+3]==true)
            {
                return PLAYER1;
            }
            if (blueboard[r][c]==true && blueboard[r+1][c+1]==true && blueboard[r+2][c+2]==true && blueboard[r+3][c+3]==true)
            {
                return PLAYER2;
            }
        }
	return 0;
  }

  void Game:: winningboard(char board[][7], bool blueboolboard [][7], bool redboolboard[][7])
  {
	  char letter;
	  cout<<endl<<endl<<endl<<endl;
	  for (int x=0; x<6; x++)
		{
		cout<<setw(5);
		for (int y=0; y<7; y++)
		{
			if (blueboolboard[x][y])
			{
				setcolor(BLACK, LIGHT_BLUE);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
			else if (redboolboard[x][y])
			{
				setcolor(BLACK, RED);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
			else
			{
				setcolor(BLACK, YELLOW);
				cout<<board[x][y]<<' ';
				setcolor(BLACK, WHITE);
			}
		}
			cout<<endl;
		}
	setcolor(BLACK, BRIGHT_BLUE);
	letter=219;
	cout<<"    "<<letter<<"           "<<letter<<endl;
	letter=30;
	cout<<endl;
	setcolor(BLACK, WHITE);
  }
