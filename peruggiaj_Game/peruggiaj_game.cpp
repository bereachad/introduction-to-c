// Course: CSC 226 Introduction to Programming with C++ 
// Name: Chad Peruggia
// Final Project: < Made a Game of connect four, simulation graphics using a character array of astericks > 
/* 
    Purpose: <Made a game of connect four.  The Game uses boollean functions to test for winners and what color
             to print on the game board.  Two player game.  Enjoy.>
*/

#include<iostream.h>
#include<windows.h>
#include<cstdlib>
#include<iomanip.h>
#include <conio.h>
#include"connectfour.h"


main()
{
    cout<<"Welcome to Connect Four !!"<<endl<<"Use the arrow keys to move, enter to drop"<<endl<<endl;   
	Game newGame;
	newGame.setcolor(BLACK, WHITE);
    newGame.run();
	system("cls");
}
