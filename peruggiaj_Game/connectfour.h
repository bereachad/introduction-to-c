#include<iostream.h>
#include<windows.h>
#include<cstdlib>
#include<iomanip.h>
#include <conio.h>

#define YES 1
#define NO 0
#define PLAYER1 1
#define PLAYER2 2
#define ENTER 13
#define SPACE 32
#define BACK 8
#define ESC 0x1b
#define LEFT 0x4b
#define RIGHT 0x4d


#ifndef CONNECT_H
#define CONNECT_H
//size of the name arrays
const int size=50;

enum Colors{ BLACK, BLUE, GREEN, TEAL, MAROON, PURPLE, MUSTARD, LIGHT_GRAY, GRAY, BRIGHT_BLUE, LIME, LIGHT_BLUE, RED, FUCHSIA, YELLOW, WHITE};

class Game{
    public:
        Game();
        void run();
        void setcolor(int, int);
        void resetboard(char[][7]);
        void resetbool(bool [][7], bool [][7], bool [][7]);
        void viewchar();
        void displayboard(char[][7], bool[][7], bool[][7], char[], const int, const int);
        int getboard(char[][7], bool [][7], bool[][7], bool[][7], char[], const int, const int);
        void falling(char[][7], bool[][7], bool[][7], bool[][7], int, char[], int, const int);
        int win(bool [][7], bool [][7]);
        void winningboard(char[][7], bool[][7], bool[][7]);

    private:
        //BOOLEAN DOUBLE SUBSCRIPTED ARRAY
        //keeps track of reds positions on the board
        bool redboolboard[6][7];
        //keeps track of blues positions on the board
        bool blueboolboard[6][7];
        //an array of the star characters
        bool cboard[6][7];
        char playerone[size], playertwo[size]; // player names
        //a string of star charcters
        char board[6][7];
        bool stop;
        bool quit;
        //INTERGERS
        //the position of the piece that will fall
        int arrowpos;
        //the value getboard returns
        int number;
        //in the for loop to convert names to uppercase
        int d;
        //to see if you want to continue
        int yesorno;
        //in the for loop to determine the pieces position
        int x;
};

void viewchar();

#endif
