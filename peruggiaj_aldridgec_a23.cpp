#include<iostream>
#include<cstdlib>
#include<string>

using namespace std;

class TicTak{

    public:

        TicTak(); // constructor sets all variables to 0 an fills each space with a ' '.
        friend ostream& operator << (ostream& outs, const TicTak& out_object);
        //precondition: The Object has been created.
        //postcondition: The object will be printed to the screen.

        void make_move();
        //precondition: There is no winner and there are still spaces to be filled.
        //postcondition: The user will input r / c, and teh space will be filled.  If the space is filled they
        // will be asked to fill it again.
        bool win();
        //precondition: gameboard has been made
        //postcondition: will tell if there is a winner.  True if there is, false if none
        int getTurns();
        // will return number of turns that has happened.
        char getWinner();
        // will return the winning move, either an X or O

    private:
        char board[3][3];
        char winner; // will fill with x or o depending who wins.
        int turn;  // turn counter, game can be palyed till turn ==8.


};


int main()
{
    int totalTurns(0);
    char win;
    cout<<"**************************************\n"
            <<"***   WELCOME TO TIK TAK TOE!!!    ***\n"
            <<"**************************************\n"<<endl<<endl;

    TicTak newGame;
     cout << newGame;
    do {
        newGame.make_move(); // fucntion to make move
        cout<<newGame; // print board
        totalTurns = newGame.getTurns(); // get totalTurns
    }while( (newGame.win()==false) && (totalTurns < 9) ); // while there is no winner and turns can be made

    win = newGame.getWinner();  // get the person who made the winning move.

    if (win=='X') // Player 1 wins
        cout << "PLAYER ONE WINS!!"<<endl<<endl;
    else if (win =='O')  // Player 2 Wins
        cout<<"PLAYER TWO WINS!!"<<endl<<endl;
    else  // Tie game
        cout <<"TIE GAME!!"<<endl<<endl;

system("PAUSE");
return 0;

}

void TicTak::make_move(){
    int r, c;

    cout<<"Where would you like to move ( row / col): ";
    cin>> r >> c;

    if ((r < 3 && c < 3)  && (board[r][c]==' ')) {
        if (turn %2==0) // player 1's turn
                board[r][c]='X';
        else// player 2's turn
                board[r][c]='O';
        turn ++; // a turn has been completed, make move.
    }
    else{
        cout <<"Invalid Turn, please enter a new move"<<endl;
        }
}

TicTak::TicTak(){ // fills the board game, each unit being filled with a blank character.
    turn=0;
    winner=' ';
    for (int r=0; r < 3; r++)
        for(int c=0; c<3; c++)
            board[r][c]=' ';
}

bool TicTak::win(){

    if ( board[0][0]==board[0][1] && board[0][0]==board[0][2] && board[0][0]!=' ' ){  // check for 1st row
        winner=board[0][0];
        return true;
    }

    if ( board[1][0]==board[1][1] && board[1][0]==board[1][2] && board[1][0]!=' '){ // check for 2nd row
        winner=board[1][0];
        return true;
    }

    if ( board[2][0]==board[2][1] && board[2][0]==board[2][2] && board[2][0]!=' '){ // check for 3rd row
        winner=board[2][0];
        return true;
    }

    if ( board[0][0]==board[1][0] && board[0][0]==board[2][0] && board[0][0]!=' '){ // check for first column
        winner=board[0][0];
        return true;
    }

    if ( board[0][1]==board[1][1] && board[0][1]==board[2][1] && board[0][1]!=' '){  // check for 2nd column
        winner=board[0][1];
        return true;
    }

    if ( board[0][2]==board[1][2] && board[0][2]==board[2][2] && board[0][2]!=' ' ){  // check for 3rd column
        winner=board[0][2];
        return true;
    }

    if ( board[0][0]==board[1][1] && board[0][0]==board[2][2] && board[0][0]!=' '){  // check diagnal
        winner=board[0][0];
        return true;
    }

    if ( board[2][0]==board[1][1] && board[2][0]==board[0][2] && board[2][0]!=' ' ){  // check diagnal
        winner=board[2][0];
        return true;
    }

    return false;
}


ostream& operator << (ostream& outs, const TicTak& out_object){
// function prints the board game
        outs << " _________________\n";
        outs << "|     |     |     | \n";
        outs << "|  "<<out_object.board[0][0]<< "  |  "<<out_object.board[0][1]<<"  |  "<<out_object.board[0][2]<<"  |\n";
        outs << "|_____|_____|_____|\n";
        outs << "|     |     |     |\n";
        outs << "|  "<<out_object.board[1][0]<<"  |  "<<out_object.board[1][1]<<"  |  "<< out_object.board[1][2]<<"  |\n";
        outs << "|_____|_____|_____|\n";
        outs << "|     |     |     |\n";
        outs << "|  "<<out_object.board[2][0]<<"  |  "<<out_object.board[2][1]<<"  |  "<<out_object.board[2][2]<<"  |\n";
        outs << "|_____|_____|_____|\n";

    return outs;
}

int TicTak::getTurns(){
    return turn; // return how many turns have occured
}

char TicTak::getWinner(){
    return winner; // returns the winning character, either an X or an O
}
