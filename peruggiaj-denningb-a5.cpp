// Course: CSC 226 Introduction to Programming with C++ 
// Name: Chad Peruggia, Brandon Denning
// Assignment 5: tOGGLE
/* 
    Purpose: This program will allow the user to input a character(lower or upper)
	and it will then convert it to the corresponding upper or lower case character
	It also filters out invald inputs.   The program was completed in class so there
	was no need to seperate 8files.>
*/



#include<iostream>
using namespace std;


char test(char input);

int main()
{
char again;

cout<<"Welcome to the tOGGLE cASE program"<<endl;
do{    
char user_input;
cout<<"Please enter a single letter:";
cin>>user_input;
cout<<endl;

//this will filter invalid inputs and prompt for a new entry
 while((user_input<'A')|| ((user_input>'Z')&&(user_input<'a'))||(user_input>'z'))
{
   cout<<"Please enter a valid input. ";
   cin>>user_input;
   cout<<endl;
}

user_input=test(user_input);
cout<<"Your input coverted is: "<<user_input<<endl;

cout<<"Would you like to do it again?";
cin>>again;
cout<<endl;
cout<<endl;


}while(again=='y'||again=='Y');

cout<<"Thank you for using tOGGLE cASE"<<endl;
cout<<"Please feel free to use it again"<<endl;
system("PAUSE");
return(0);

}

char test(char input)
{
 	
  if ((input>='A')&& (input<='Z'))
    {
        input=(input+32);//adds 32 to convert it to its lowercase 
        return input;
    }
    else{
     input=(input-32);//subtracts to convert to uppercase value
        return input;
    }

}

