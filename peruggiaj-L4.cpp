// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Lab L4: Using an array to work with UPC Codes
/*
    Purpose: <This program allows the user to input a source file containing UPC codes and will tell you if they
    are valid as well as print them out in binary.  It makes use of arrays , classes as well as a global variable.>
*/

#include<iostream>
#include<cmath>
#include <fstream>

using namespace std;

const int MAX_SIZE = 13;  // array can only be of size 13.

class BarCodes
{
    public:
        BarCodes(); // create an empty array
        void add_digit (int digit); // add a value scanned into the array
        void input_values( istream & inputfile);
    // precondtion: The user said to either input file or input cin
    // postcondition: the array is filled using the respective stream.
      void output(ofstream& outs);
      // precondition : the array has been filled
      // postcondtion : The array is printed to set stream

    void case_begginging(int digit, ofstream& outputfile); // prints the first half of the UPC binary
    void case_ending(int digit, ofstream& outputfile); // prints the second half of the UPC binary
    bool full () const;

    private:
        int size; // Size of array that increments every time a value is added and makes sure it isn't to big
        int UPC[MAX_SIZE];  // makes an array called UPC of size 13
        bool check_valid();

};


int main()
{
    char input_name[20], output_name[20];
    int num_barcodes;
    int menu_choice= 0, find, same;

    BarCodes UPC;

    cout <<"Welcome to the BarCode Checker"<<endl;

    ifstream input;  // set input stream to input
    ofstream output;  // set output stream to output

    cout<<"What is the name of the input file? ";
    cin>>input_name;
    input.open(input_name);

                while (input.fail()) // if file can't open will tell the user and will ask for a new one
                {
                    cout<<"Open "<<input_name<< " failed. Please try again\n"<<endl;
                    input.close();
                    input.clear();  // clears the input flag
                    cout<<"File Name: ";
                    cin>>input_name;
                    input.open(input_name);
                }
        cout << "What is the output file name? ";
        cin >> output_name;
        output.open(output_name);
        input>>num_barcodes; // scans in teh first line of the text file, which is how many barcodes are in the file.  Needed for the loop.

        for (int r = 0;  r  < num_barcodes;  r++)  // loop to scan and print for each barcode
       {
               UPC.input_values(input);
               UPC.output(output);
                UPC=BarCodes(); // clears out the UPC array.
        }
input.close();
output.close();

return 0;
}

    BarCodes::BarCodes() : size(0)
    {
        //Body intentionally empty.
    }

    void BarCodes::add_digit(int digit)
    {//Uses iostream and cstdlib:
        if ( full( ) )
        {
            cout << "Error: adding to a full array.\n";
        }
        else
        {
            UPC[size] = digit;
            size = size + 1;
        }
    }

    bool BarCodes::full( ) const
    {
         return (size == MAX_SIZE);
    }

bool BarCodes::check_valid()
{
    int oddTotal(0), evenTotal(0), checkNum(0), total(0);

        for (int o=0; o< 11; o= o+2) // takes the odd spaced array places and adds tehm together.
            oddTotal = oddTotal + UPC[o];
        for (int e =1; e < 11; e=e+2) // takes the even spaced array places and adds them together.
            evenTotal= evenTotal + UPC[e];

    total = (oddTotal*3) + evenTotal;  // sets the total to be odd *3 + even
    total = 10 - (total % 10) ;  // sets total to be 10 - remainder after division

return (total==UPC[11]);  // return if it is valid or not.
}

void BarCodes::input_values( istream& inputfile) // fucntion to fill array
{
        int digit;
        for (int x =0;x <12 ; x++){  //loop to fill while less then 12, since the array is from 0 - 11
                inputfile  >>digit;
                add_digit(digit); // call teh function to add the digit to the array
            }
        }

void BarCodes::output(ofstream& outputfile)  // fucntion will print to the parameter of outputfile, either cout or a file name
{
    //check_valid();
   if (check_valid() == true) // the formual returned true
    {
            outputfile<<"101 "; // guard

            for (int t=0 ; t<6; t++)  // first half is units 0 - 5
                case_begginging(UPC[t], outputfile);

            outputfile << "01010 "; // CENTER

            for (int i=6; i<12; i++) // second half is units 6 - 11
                case_ending(UPC[i], outputfile);

            outputfile <<"101 "; // guard
            outputfile <<endl;
      }
    else
      outputfile << "ERROR THE CHECK NUMBER IS NOT CORRECT!"<< endl;

}

void BarCodes::case_begginging(int digit, ofstream& outputfile)
// swtich statement function placed here to increase readability.  Just prints depending on what case
{
    switch (digit)
    {
        case (0):
            outputfile << "0001101 ";
            break;
        case (1) :
            outputfile << "0011001 ";
            break;
        case (2):
        outputfile << "0010011 ";
            break;
        case (3):
            outputfile << "0111101 ";
            break;
        case (4):
        outputfile << "0100011 ";
            break;
        case (5):
        outputfile << "011001 ";
            break;
        case (6):
        outputfile << "0101111 ";
            break;
        case (7):
        outputfile << "0111011 ";
            break;
        case (8):
        outputfile << "0110111 ";
            break;
        case (9):
        outputfile << "0001011 ";
            break;
    }
}

void BarCodes::case_ending(int digit, ofstream& outputfile)
// swtich statement function placed here to increase readability.  Just prints depending on what case
{
    switch (digit)
    {
        case (0):
            outputfile << "1110010 ";
            break;
        case (1) :
            outputfile << "1100110 ";
            break;
        case (2):
        outputfile << "1101100 ";
            break;
        case (3):
            outputfile << "1000010 ";
            break;
        case (4):
        outputfile << "1011100 ";
            break;
        case (5):
        outputfile << "1001110 ";
            break;
        case (6):
        outputfile << "1010000 ";
            break;
        case (7):
        outputfile << "1000100 ";
            break;
        case (8):
        outputfile << "1001000 ";
            break;
        case (9):
        outputfile << "1110100 ";
            break;
    }
}
