#include<iostream>
#include <string>

using namespace std;

class Fraction{
    public:
        Fraction();
        Fraction(int numer);
        Fraction(int numer, int denom);

        friend Fraction operator +(const Fraction fraction1,const Fraction fraction2);
        //precondition: Two fraction objects have been made
        //post condition: will add the two objects
        friend  Fraction operator -(const Fraction fraction1,const Fraction fraction2);
        //precondition: Two fraction objects have been made
        //post condition:   will subtract the two objects
        friend  Fraction operator *(const Fraction fraction1,const Fraction fraction2);
        //precondition: Two fraction objects have been made
        //post condition:   Will multiply the objects
        friend Fraction operator /(const Fraction fraction1,const Fraction fraction2);
        //precondition: Two fraction objects have been made
        //post condition:   Will divide the objects.
        friend Fraction operator - (const Fraction fractionselected);
        //precondition: Object has been created
        // postcondition: will show the negative value of that object.
        friend bool operator ==(const Fraction fraction1,const Fraction fraction2);
        //Precondition: Have two fractions to test equality on
        //PostCondition: Cross multiplies the two fractions to test if the fractions are equal or not.
        friend bool operator <(const Fraction fraction1,const Fraction fraction2);
        //Precondition: Have two fractions built with denom and numenator.
        //PostCondition: turns it into a decimal and finds if less then.
        friend bool operator >(const Fraction fraction1,const Fraction fraction2);
        //Precondition: Have two fractions built with denom and numenator.
        //PostCondition: turns it into a decimal and finds if greater then.
        friend bool operator >=(const Fraction fraction1,const Fraction fraction2);
        //Precondition: Have two fractions built with denom and numenator.
        //PostCondition: turns it into a decimal and finds if greater than or equal to.
        friend bool operator <=(const Fraction fraction1,const Fraction fraction2);
        //Precondition: Have two fractions built with denom and numenator.
        //PostCondition: turns it into a decimal and finds if less then or equal to.

        friend istream& operator >>( istream& ins, Fraction& in_object );
        friend ostream& operator << (ostream& outs, const Fraction& out_object);

        friend double get_decimal(const Fraction fraction1);
        //Precondition: have a denominator and a numorator in the object
        //Postcondition: Divides the fraction to recieve a decimal.

    private:
        int numerator;
        int denominator;
};

void menu_list();
//postcondition: will display initial menu
void bool_menu();
// postcondition: will display boolean menu

int main(){

    Fraction f1, f2;
    int menu, boolmenu;
    cout<<"Enter fraction 1: ";
    cin>>f1;
    cout<<"Enter Fraction 2: ";
    cin>>f2;

    // switch menu loop
    do{
        cout <<endl;
        menu_list();
        cin>>menu;
        switch (menu){
            case (1):
                cout <<"Sum is "<< f1+f2<<endl;
                break;
            case (2):
                cout<<"Difference is:"<<f1-f2<<endl;
                break;
            case (3):
                cout<<"The product is : "<<f1*f2<<endl;
                break;
            case (4):
                cout<<"The quotient is : "<<f1/f2<<endl;
                break;
            case (5):
                cout << "Fraction 1 as a Negative is :"<<-(f1)<<endl;
                break;
            case (6):
                cout << "Fraction 2 as a Negative is :"<<-(f2)<<endl;
                break;

            case (7):
                cout<<"Boolean Tests\n";
                bool_menu();
                cin>>boolmenu;
                switch (boolmenu){ // menu for various boolean tests.
                    case(1):
                        cout<<" Are they equal?";
                        if(f1==f2)
                            cout<<"\n They are equal!"<<endl;
                        else
                            cout<<"\nThey are not equal!!"<<endl;
                        break;
                    case(2):
                        cout<<"Is fraction 1 less than fraction 2?";
                        if(f1<f2)
                            cout<<"\nIt is!"<<endl;
                        else
                            cout<<"\nNope"<<endl;
                        break;
                    case(3):
                        cout<<"Is fraction 1 greater than fraction 2?";
                        if(f1>f2)
                            cout<<"\nIt is!"<<endl;
                        else
                            cout<<"\nNope"<<endl;
                        break;
                    case(4):
                        cout<<"Is fraction 1 less than or equal to fraction 2?";
                        if(f1<=f2)
                            cout<<"\nIt is!"<<endl;
                        else
                            cout<<"\nNope"<<endl;
                        break;
                    case(5):
                        cout<<"Is fraction 1greater than or equal to fraction 2?";
                        if(f1>=f2)
                            cout<<"\nIt is!"<<endl;
                        else
                            cout<<"\nNope"<<endl;
                        break;
                    default:
                        break;
                }
                break;
            case (8):
                exit(1);
                break;
            default:
                cout<<"Invalid Choice! Exiting"<<endl;
                break;
        }
    }while (menu>=0 && menu <9);
}


void menu_list(){  // menu for switch statement
    cout<<"1: Add the fractions\n"<<"2: Subtract the fractions \n"<<"3: Multiply the fractions\n"
            <<"4: Divide the Fractions\n"<<"5: Show Fraction 1 as a Negative\n"<<"6: Show Fraction 2 as a Negative\n"<<
            "7: Boolean Tests\n"<<"8: Exit\n"<<endl<<"Choice: ";
}

void bool_menu(){
    cout<<"1: Test to see if they are equal\n"<<"2: Fraction 1 < Fraction 2\n"<<"3: Fraction 1 > Fraction 2\n"
            <<"4: Fraction 1 < = Fraction 2\n"<<"5: Fraction 1 >= Fraction 2\n"<<"Choice: ";
}

 istream& operator >>( istream& ins, Fraction& in_object ){
    int top, bot(0), pos(0);  // integers to use in the fraction
    string upper, bottom, fullfraction;  // strings for user input
    getline(ins,fullfraction);// get the line that the user enters
    pos = fullfraction.find('/', pos);  // find a / in that line
    if (pos < fullfraction.length() && pos>0){  // if there is a / within the size of the string
        upper=fullfraction.substr(0,pos);  // the upper part is from the start till the /
        bottom=fullfraction.substr (pos+1);  // bottoms if from the spot after the / till the end
        top=atoi(upper.c_str());  // convert the strings to integers
        bot=atoi(bottom.c_str());
            if (bot==0){  // if the denominator is 0
                cout<<"Denominator is 0.  Can't do this!!! Exiting";
                exit (1);
            }
        in_object=Fraction(top, bot);
    }
    else{  // there is no /
        top=atoi(fullfraction.c_str());  // the top is the integer value of the string
        in_object=Fraction(top);
    }
}

ostream& operator << (ostream& outs, const Fraction& out_object){
    outs << out_object.numerator<<"/"<<out_object.denominator;
    return outs;
}

Fraction::Fraction(){  // default constructor will make it 1/1
    numerator= 1;
    denominator=1;
}

Fraction::Fraction(int numer){
    numerator=numer;
    denominator=1;
}

Fraction::Fraction(int numer, int denom){
    numerator=numer;  // private variable set to parameter that was passed in
    denominator=denom; // private variable set to parameter that was passed in
}

Fraction operator + (const Fraction fraction1,const Fraction fraction2)
{
       int numer, denom;
       numer=((fraction1.numerator*fraction2.denominator)+(fraction2.numerator*fraction1.denominator));
       //adds the result of the denominator
       denom=(fraction1.denominator*fraction2.denominator); //finds a denominator
       Fraction temp(numer, denom);//Creates a temporary object for the answer.
       return temp; //return the temporary object.
}

Fraction operator -(const Fraction fraction1, Fraction fraction2)
{
       int numer, denom;
       numer=((fraction1.numerator*fraction2.denominator)-(fraction2.numerator*fraction1.denominator));
       //subtracts the result of the denominator
       denom=(fraction1.denominator*fraction2.denominator); //finds a denominator
       Fraction temp(numer, denom);//Creates a temporary object for the answer.
       return temp; //return the temporary object.
}

 Fraction operator *(const Fraction fraction1,const Fraction fraction2)
{
       int numer, denom;
       numer=(fraction1.numerator*fraction2.numerator); //multiply both tops
       denom=(fraction1.denominator*fraction2.denominator); //multiply both bottoms
       Fraction temp(numer, denom);//Creates a temporary object for the answer.
       return temp; //return the temporary object.
}

 Fraction operator /(const Fraction fraction1,const Fraction fraction2)
{
       int numer, denom;
       numer=(fraction1.numerator*fraction2.denominator); //cross multiply
       denom=(fraction1.denominator*fraction2.numerator); //cross multiply
       if (denom==0){
            cout<<"0 on a demoninator, Error!\n"<<"Exiting Program\n";
            exit(1);
        }
       Fraction temp(numer, denom);//Creates a temporary object for the answer.
       return temp; //return the temporary object.
}

Fraction operator - (const Fraction fractionselected){
    Fraction temp((-1*(fractionselected.numerator)), (fractionselected.denominator));  // sets a temp object to have the negative numerator
    return (temp);
}

bool operator ==(const Fraction fraction1,const Fraction fraction2)
{ //returns the values of cross multiplication to test the equality of the fractions
    return(get_decimal(fraction1)==get_decimal(fraction2));
}

 bool operator <(const Fraction fraction1,const Fraction fraction2)
{ //tests to see if the first fraction is less than second.
    return((get_decimal(fraction1))<get_decimal(fraction2)); //return the boolean value of less than.
}

bool operator >(const Fraction fraction1,const Fraction fraction2)
{ //tests to see if the first fraction is greater than second.
    return(get_decimal(fraction1)>get_decimal(fraction2)); //return the boolean value of greater than.
}

 bool operator <=(const Fraction fraction1,const Fraction fraction2)
{ //tests to see if the first fraction is greater than or equal to second.
    return(get_decimal(fraction1)<=get_decimal(fraction2)); //return the boolean less than or equal to
}

 bool operator >=(const Fraction fraction1,const Fraction fraction2)
{ //tests to see if the first fraction is less than or equal to second.
    return(get_decimal(fraction1)>=get_decimal(fraction2)); //return the boolean value of greater than or equal.
}

double get_decimal(const Fraction fraction1)
{
    double temp; //temporary return value
    temp=(fraction1.numerator/fraction1.denominator);//changes it into a decimal.
    return temp;//returns the decimal
}
