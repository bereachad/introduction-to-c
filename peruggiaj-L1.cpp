// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
//Contributions:  Matt Ondak noticed the bug of which if a negative # of sticks
//was entered, it would still run the game and add to the num of sticks left.
// Lab #1: The game of NIM
/* This lab will allow you to play a game of "Nim" with the computer.  You
can pick any number of initial sticks over 9 and pick up to 4 sticks at a time */

#include <iostream>
#include <cstdlib>
using namespace std;
int main(){
   char again;
    cout<<"PICK UP STICKS!!!!"<<endl;
	cout<<"You will be playing a game of Pickup Sticks (NIM) vs. a computer.  The computer"
	<<" is pretty smart, so be on your toes and good luck!"<<endl;
do{
	   //declare variables:
	int turn(0), num_sticks(0),rem_sticks(0),take_sticks(0);
    cout<<"\n"<<"Please enter a number greater than 9 and less then 50 for initial sticks:";
    cin>>num_sticks;cout<<"\n";
    //make sure number of initial sticks is valid, will accomplish this by running
    //a loop to repeat input if the number is not valid
    while ((num_sticks>=50)||(num_sticks<=8)){
       cout<<"Please enter a initial number greater than 9 and less then 50:";
       cin>>num_sticks;
       cout<<"\n";
 }
//loop to allow play to conintue if sticks !=0
while(num_sticks>0) {
   while(turn==1){//computers turn takes 1 away to make it 0, meaning players turn
	  turn--;//take one from turn making it 0, forcing a player turn next
   	  int comp_take(1), rem(0),dummy_sticks;
/*dummy sticks is a placeholder to test.  if dummy sticks divides by 5 with no remainder
the comp will want to make that move, it will keep adding on to comp_take to try and accomplish
this, if not possible, the computer will take 1 stick*/
   dummy_sticks=num_sticks-comp_take;
   rem=dummy_sticks%5;
	   while(rem!=0){
	 	 			if(comp_take<=3){
 					 	comp_take++;
						dummy_sticks=(num_sticks)-(comp_take);
						rem=dummy_sticks%5;
					}
					else{
						rem=0;//force to 0 to make it leave the loop
						comp_take = (rand()%3)+1;
					}
		 }
	    num_sticks=(num_sticks)-(comp_take);
	   cout<<"The Computer took"<<"  "<<comp_take<<" "<<"sticks"<<endl;
   }
   while ((num_sticks>0)&&(turn==0)){
    turn++;//add one to turn, making it 1, forcing a computer turn
    cout<<"\n"<<"There are:"<<"  "<<num_sticks<<" "<<"remaining"<<endl;
    cout<<"Enter how many sticks you would like to take (1 -4):";
    cin>>take_sticks;
    while((take_sticks>4)||(take_sticks<0)){
	      cout<<"\n"<<"You Cheater! Please take only 1 to 4 sticks.  Enter Again:";
 		  cin>>take_sticks;
 		  cout<<endl;
		   }
   if (take_sticks >num_sticks){
	   			   cout<<"You took to many sticks, there aren't even that many left!"<<
	   			   " Enter the amount again!: ";
	   			   cin>>take_sticks;
	   			   cout<<endl;
   				   }
    num_sticks=num_sticks-take_sticks;
    cout<<"\n"<<"There are:"<<"  "<<num_sticks<<" "<<"remaining"<<endl;
	}
}
	if (turn==1)
	   cout<<"\n"<<"YOU WIN!!!!"<<endl;
	else
		cout<<"\n"<<"Computer WINS!!!"<<endl;
cout<<endl;
cout<<"Would you like to play again?(Y/N)";
cin>>again;cout<<endl;
}while(again=='y'||again=='Y');
return(0);}
