// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 17: <Program using array classes.>
/*
    Purpose: <This program gives the user a list of options for manipulating an Array class object.  They can either print it,
                        fill it, or find if a number occurs in the array and how many times.>
*/


#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

  const int MAX_LIST_SIZE = 500;  //Max array size

  class createArray
  {
  public:
     createArray();
     //Initializes the object to an empty array.

     int get_size(); // will pass the size variable for use in teh thrid menu option

     void add_value(double value);
     //Precondition: The array is not full.
     //Postcondition: The value has been added to the list.

     double get_value(int i);
     //Precondition: The list has data in the indexed location i.
     //Postcondition: The value has been returned and printed

     int scanner( int x);
     // precondition: the user told what number they want to find
     // postcondition: returns the number of tiems that number was found

    void input_values( istream & instreams);
    // precondtion: The user said to either input file or input cin
    // postcondition: the array is filled using the respective stream.

      void output(ostream& outs);
      // precondition : the array has been filled
      // postcondtion : The array is printed to set stream

     bool full( ) const;
     //Returns true if the list is full, false otherwise.

  private:
     double arrayTest[MAX_LIST_SIZE]; //array of numbers
     int size; //number of array positions filled
  };

void welcome();  // fucntion delcaration for a welcome (menus)

  int main()
  {

      char input_name[20], output_name[20], ans;
        int menu_choice= 0, find, same;
    cout <<"Welcome to the Array Creation / Display program\n"<<endl;
    ifstream input;  // set input stream to input
    ofstream output;  // set output stream to output
    createArray arrayMade;  // make an array of arrayMade name

    do {  // open loop so they can keep going as long as they dont choose break 6
        welcome();
            //do{
            cout <<"\nWhat option would you like to do? ";
            cin>> menu_choice;
           //} while((menu_choice < 0 || menu_choice > 6 ));

        switch(menu_choice)
        {
            case(0): // leave loop
                break;

            case(1):
                arrayMade=createArray();  // clear out the array
                arrayMade.input_values(cin);   // function to read from keyboard
            break;

            case (2):
                 // function to read and fill from file
                 arrayMade=createArray(); // clear out array
                 cout <<"What is the files name? ";
                 cin>>input_name;
                 input.open(input_name);
                if (input.fail()){  // if file can't open will tell the user and will ask for a new one
                    cout<<"Open "<<input_name<< " failed. Please try again\n"<<endl;
                    input.close();
                    input.clear();
                    break;
                }
                arrayMade.input_values(input);  // will input into the array

                cout<<endl<<arrayMade.get_size()<<" Units in this array. \n"<<endl;

                break;

            case (3):  // case to search the array for a certain number
                cout<<"\nWhat number would you like to search for? ";
                cin>>find;  // user input for number to find
               same = arrayMade.scanner(find);  // sets same to the variable of the scanner counter
               cout<<"\nThat number occurs "<< same <<" times.\n"<<endl;
                break;

            case (4):
                // print the array to the screen
                arrayMade.output(cout);
                break;

            case (5):
            // write to a file
                cout<<"What file would you like to write to?";
                cin>>output_name;
                output.open(output_name);  // opens a file of selected name
                arrayMade.output(output);  // makes it cout to output, which is our file stream name
                cout<<endl;
                break;

            case (6):  // end the program
                    cout<<"Thank you for using the program"<<endl;
                    system("PAUSE");
                    exit (1);
                break;
        }
    }while(menu_choice >0 && menu_choice < 6 );

    input.close ();
    output.close();
    system("PAUSE");
	return 0;
  }

void welcome()
{
    cout<<"Chose one of the following below:\n";
    cout <<endl;
    cout<<"1: Read from the keyboard"<<endl<<"2: Read from a file"<<endl<<"3: Search the array"<<endl
            <<"4: Write to the screen"<<endl<<"5: Write to a file"<<endl<<"6: Quit the program"<<endl;
}


//-----------------------------------------
//  Start fucntion defintions for the class
//----------------------------------------
    createArray::createArray() : size(0)
    {
        //Body intentionally empty.
    }

    void createArray::add_value(double value)
    {//Uses iostream and cstdlib:
        if ( full( ) )
        {
            cout << "Error: adding to a full array.\n";
            system("PAUSE");
            exit(1);
        }
        else
        {
            arrayTest[size] = value;
            size = size + 1;
        }
    }

    double createArray::get_value(int i)
        {
        return (arrayTest[i]);
        }


    bool createArray::full( ) const
    {
         return (size == MAX_LIST_SIZE);
    }

int createArray::scanner(int x) // class function to see if any items in teh array are the same, if so adds one to the count
{
    int same (0);  // local variable, will be cleared every single time
    for (int position=0; position < size; position++)
        {
            if (x == (get_value(position)))
                same ++;
        }
    return same;
}

int createArray::get_size()
{
    return size;
}

void createArray::input_values( istream& inputfile)
{
    int number;
    int max_size;
    inputfile >> max_size;  // first input will be size of teh array
    for ( int x= 0;  x < max_size; x++){
        inputfile >> number;  // takes the stream and sets it to number
        add_value(number); // passes number as a variable to add_value function to place it in teh array
    }
}

void createArray::output(ostream& outputfile)  // fucntion will print to the parameter of outputfile, either cout or a file name
{
    outputfile<<"Array is of size: "<<size;
    outputfile <<endl;
    for (int x=0; x<size; x++)
    {
        outputfile<< get_value(x)<<" ";
    }
    outputfile<<endl<<endl;
}
