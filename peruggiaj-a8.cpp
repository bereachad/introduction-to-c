// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia  (or both names if you are working with a partner)
// Assignment 8: <This is a nut game where you can guess what shell the nut is under>
/*
    Purpose: <This program makes use of random integers, and reference parameters.  The user can
    input starting amount of money and bet per turn to see if htey can guess the shell which contains the nut.
    If they do, they get the bet added to the amount initally, if not they loose that bet.>
*/


#include <iostream>
#include <cstdlib>

using namespace std;
void gamble(int user_pick, int& money, int bet, int& turn_left);//function to gamble and play
void shuffle(bool& shell1, bool& shell2, bool& shell3 );//function to see what shell contains nut


int main()
{

    int turns(10),start_money(0), bet_money(0), guess(0);
    cout<<"********************************************"<<endl;
    cout<<"Welcome to the guess the shell game! The shell will appear in one of three shells"<<endl;
    cout<<"that you are required to guess! Good Luck!"<<endl
    <<"*********************************************"<<endl;

    cout<<"How much money would you like to start with?";cin>>start_money;cout<<endl;

    do{
    cout<<"How much would you like to bet?";cin>>bet_money;cout<<endl;//input bet
    cout<<"What shell has the nut( 1 - 3)?";cin>>guess;cout<<endl;//input guess of nut
        while((guess<1)||(guess>3))//loop to make sure guess was correct.
        {
            cout<<"Please pick shell 1 - 3"<<guess<<" :";
            cin>>guess;
        }
    gamble(guess, start_money, bet_money,turns);//sends guess, start money and bet as well as turns to the gamble function
    }while((start_money!=0)&&(turns>=1));

    system("PAUSE");
    return (0);
}

//gamble function
void gamble(int user_pick, int& money, int bet, int& turn_left) // money and turn_left have to be modified from its original
{
    bool shell1=false, shell2=false, shell3=false; //declare 3 boolleans to false for shuffle funtion
    shuffle(shell1,shell2,shell3); // shuffle three false booleans

    if ((money==0)||(bet>money))
        {
            cout<<"You can't bet that amount! Enter a new valid bet up to"<<money<<" and not 0:";
            return;
        }
    else{

    if ((user_pick==1)&&(shell1==true)) //if your pick is 1 and shell1 is now true, you guess right
    {
        money=money+bet;
        cout<<"You won!! Good Job!  You now have $"<<money<<endl;
    }
    else if ((user_pick==2)&&(shell2==true))//if your pick is 2 and shell2 is now true, you guess right
    {
        money=money+bet;
        cout<<"You won!! Good Job!  You now have $"<<money<<endl;
    }
    else if ((user_pick==3)&&(shell3==true))//if your pick is 3 and shell3 is now true, you guess right
    {
        money=money+bet;
        cout<<"You won!! Good Job!  You now have $"<<money<<endl;
    }
    else // you lost
    {
        money=money-bet;
        cout<<"You Lost!!It was under a different shell. You now have $"<<money<<endl;

    }
    turn_left--; // take one away from turn so the user will have used a turn
}
}

void shuffle(bool& shell1, bool& shell2, bool& shell3)
{
    double shell_1=static_cast<double>(rand())/((RAND_MAX) + 1); // random num 1
    double shell_2=static_cast<double>(rand())/((RAND_MAX) + 1);//random num 2
    double shell_3=static_cast<double>(rand())/((RAND_MAX) + 1);// random num 3

    if ((shell_1>shell_2)&&(shell_1>shell_3)) // if num 1 is better then num 2 and num 3 then it is true
        {shell1=true;
        shell2=false;
        shell3=false;
        //cout<<"1";
        }
    else if ((shell_2>shell_1)&&(shell_2>shell_3)) // if num 2 is better then num 1 and 3 then it is true
        {shell2=true;
        shell1=false;
        shell3=false;
        //cout<<"2";
        }
    else if ((shell_3>shell_2)&&(shell_3>shell_1))// if num 3 is better then 1 and 2,  then it is true.
        {shell3=true;
        shell1=false;
        shell2=false;
        //cout<<"3";
        }


}

