// Course: CSC 226 Introduction to Programming with C++ 
// Name: Cody Alann Aldridge && James Chad Anthony Peruggia
// Assignment 23: <This program is a two player Tik Tak Toe Game.>
/*
    Purpose: <This program will allow two people to play TIk Tak Toe.  We use seperate files to compile.>
*/

#include<iostream>
#include<cmath>
#include "tiktac.h"

using namespace std;

int main()
{
    int totalTurns(0);
    char win;
    cout<<"**************************************\n"
            <<"***   WELCOME TO TIK TAK TOE!!!    ***\n"
            <<"**************************************\n"<<endl<<endl;

    TicTak newGame;
     cout << newGame;
    do {
        newGame.make_move(); // fucntion to make move
        cout<<newGame; // print board
        totalTurns = newGame.getTurns(); // get totalTurns
    }while( (newGame.win()==false) && (totalTurns < 9) ); // while there is no winner and turns can be made

    win = newGame.getWinner();  // get the person who made the winning move.

    if (win=='X') // Player 1 wins
        cout << "PLAYER ONE WINS!!"<<endl<<endl;
    else if (win =='O')  // Player 2 Wins
        cout<<"PLAYER TWO WINS!!"<<endl<<endl;
    else  // Tie game
        cout <<"TIE GAME!!"<<endl<<endl;

system("PAUSE");
return 0;

}
