#include <iostream>

using namespace std;

#ifndef TIKTAK_H
#define TIKTAK_H
class TicTak{

    public:

        TicTak(); // constructor sets all variables to 0 an fills each space with a ' '.
        friend ostream& operator << (ostream& outs, const TicTak& out_object);
        //precondition: The Object has been created.
        //postcondition: The object will be printed to the screen.

        void make_move();
        //precondition: There is no winner and there are still spaces to be filled.
        //postcondition: The user will input r / c, and teh space will be filled.  If the space is filled they
        // will be asked to fill it again.
        bool win();
        //precondition: gameboard has been made
        //postcondition: will tell if there is a winner.  True if there is, false if none
        int getTurns();
        // will return number of turns that has happened.
        char getWinner();
        // will return the winning move, either an X or O

    private:
        char board[3][3];
        char winner; // will fill with x or o depending who wins.
        int turn;  // turn counter, game can be palyed till turn ==8.
};

#endif
