// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia Cody Aldridge
// Lab 3: Creating a new class which uses loops to output diamonds
/*
    Purpose: <Our program allows the user to input a number and print a diamond of that size
    We use two for loops to check for lines first, then spaces next, when the spaces weren't needed, it would go to the next
    line and repeat.  We added a nice welcome header as well as something that will tell you the area, perimeter, as well polygonal diagnoal. (Cody's Idea!)
    We live together, so we turned in the same lab.>
*/

#include <cstdlib>
#include <iostream>
#include <cmath>

class Diamond
{
  public:
  	Diamond(); 				// default constructor assumes a diamond of 1 star
    Diamond(int side) ;  // constructor for any size dice
    int NumSides() const;
    void printDiamond();  // print function
    void extrafunction(); // function to do area, outside length, and diagonal
  private:
    int side;  // variable to be used throughtout the class.
};

using namespace std;
void Welcome();

int main()
{
    int userside;
    Welcome(); // welcome function to welcome the user
    cout<<"\t\tWhat is the length to this diamond?:  "; // asks for input
    cin>>userside; // user side is the size of the diamond
    
    if (userside<=0) // if the userside is less then 0, meaning a negative number, tell them thanks and close.
    {
        cout<<"You entered an invalid number! Thank you for using this program"<<endl;
        system("PAUSE"); // pause here to read the thank you
        return(0);
    }
    
    Diamond dia(userside); // makes a diamond of the users input

    system("PAUSE");
    return EXIT_SUCCESS;
}

Diamond::Diamond()//Default is to assume a one sided diamond.
// postcondition: all private fields initialized
{
   side = 1;
   printDiamond();
   extrafunction();
}

Diamond::Diamond(int userside)
// postcondition: all private fields initialized
{
   side=userside;
   printDiamond();
   extrafunction();
}

 int Diamond::NumSides() const
// postcondition: return # of sides of diamond.
{
    return side;
}

void Diamond::printDiamond()
// postconditon: print the diamond
{
   int lines(0), spaces(0); // initialize two variables to 0.

    for(lines=-side;lines<=side;lines++){ // set the lines needed to be -side(ie if sides is 5, we need 10 so we will have it go from - 5 to postivie 5)
         cout<<"\t\t\t";
            for(spaces=-side;spaces<=side;spaces++) // set the number of spaces to be the -side. We want to do this untill the num of total spaces printed is the num of the side;
            {
                if( abs(lines)+abs(spaces)!=(side-1)) // if the sum of the two postive numbers used in the loops ( line you are on + spaces needed) is not teh same as 1 less then the side, print a space
                {
                    cout<<" ";
                }
                else  {    // if the sum is the same, this is a star location
                cout<<"*";
                }
            }
        cout<<endl; //Goes to next line because the stars have been printed and will need to move down one
    }
}

void Diamond::extrafunction()
// postcondition: will get the area, perimeter, and diagnal of the diamond and print them
    {
         int perimeter;
         double area;
         area=pow((side-1),2.0);
         perimeter=(side-1)*4;
         
         cout<<"\t\tThe Area inside The Diamond is "<<area<<".\n";
         cout<<"\t\tThe Number of stars that make up the Diamond are "<<perimeter<<".\n";

         }

void Welcome(){
cout<<"***************************************************************"<<endl;
cout<<"*****         Welcome to the Diamond Drawing Program    *******"<<endl;
cout<<"***************************************************************"<<endl<<endl<<endl;
}
