// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 22: <This program is a sample program on how to overload operators and use friend functions>
/*
*/

#include<iostream>
#include<cstdlib>
#include <string>

using namespace std;

class Percent {
 public:
   Percent(); 				// default constructor
   Percent(int percent_value);	// additional constructor

   int getPercent() const;
   // accessor function which returns the value of the member variable
   // precondition:  A Percent object has been made
   //post condition: will return the value of that percent

   void setPercent( int newpercent );
   // mutator function which sets the value of the member variable

   friend bool operator ==( const Percent& first, const Percent&second );
   // overloaded == operator which returns true only when the Percents are equal
   //precondition: There are two objects made
   //postcondition:  Will tell you if the two objects are the same

   friend istream& operator >>( istream& ins, Percent& in_object );
   // overloaded >> operator for input which consists of an integer followed by the '%'
   // precondition: an object has been made;
   //postcondition: will fill the object with the input from the stream

   friend ostream& operator <<( ostream& outs, const Percent& out_object );
   //overloaded << operator which produces output in the format: 25%
   //precondition: object has been made and filled
   //postcondition: will print the percent of the object.

 private:
   int mypercent;
};


int main()
{
    Percent per1, per2;  // creates the two objects
    int input1, input2;char per;

    cout<<"Using regular constructors and user input!!\n";
    cout<<"Enter the first percent: ";
    cin>>input1>>per;
    cout<<"Enter the Second Percent: ";
    cin>>input2>>per;

    Percent userper1(input1); Percent userper2(input2);
    if ( userper1.getPercent() == userper2.getPercent()){  // if per1 and per 2 are the same user will find out
        cout << "They are Equal!\n";
        cout <<"Percent 1:"<<userper1.getPercent()<<"%\tPercent 2:"<<userper2.getPercent()<<per<<endl;
    }
    else
    {
        cout<<"They are not Equal!\n";
        cout <<"Percent 1:"<<userper1.getPercent()<<"%\tPercent 2:"<<userper2.getPercent()<<per<<endl;
    }

    cout <<endl<<endl<<endl;

    cout <<"Operators and Friend functions used here!!\n";
    cout << "Enter the first percent: ";
    cin  >> per1;  // user enters percent one, using overloaded cin here
    cout << "Enter the second percent : ";
    cin >> per2;  // user enters percent two, using overloaded cin here
    if ( per1 == per2){  // if per1 and per 2 are the same user will find out
        cout << "They are Equal!\n";
        cout <<"Percent 1:"<<per1<<"\tPercent 2:"<<per2<<endl;
    }
    else
    {
        cout<<"They are not Equal!\n";
        cout <<"Percent 1:"<<per1<<"\tPercent 2:"<<per2<<endl;
    }
}

istream& operator >> (istream& ins, Percent& in_object)
{
    char symbol; // variable to eat the % sign
    int number; // variable to get the integer
    cin>>number>>symbol; // will get the integer and symbol
    in_object.mypercent=number;
    return ins;
}

ostream& operator << (ostream& outs, const Percent& out_object){
    outs << out_object.mypercent<<"%";
    return outs;
}

Percent::Percent(){
    //body left empty on purpose
}

Percent::Percent(int percent_value){	// additional constructor
        mypercent=percent_value;
}

bool operator ==( const Percent& first, const Percent&second ){
    return(first.mypercent == second.mypercent);
}

int Percent::getPercent() const{
    return mypercent;
}
