// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia, Nicole Lay
// Lab 2: Creating Histograms
/*
    Purpose: <This program will allow the user to tell how many choices there were
    on a respected question on the Exam.  It will then prompt them to enter how many people
    picked each choice.  Will take the results and display it in a histogram.  EXTRA FEATURE!!
    You can pick the amount of questions on the test and it will loop that many times!>
*/

#include <iostream>
using namespace std;


int valid (int num_choice);//function to make sure they entered 4 or 5 as choice
void histo(int a, int b, int c, int d,int max);//histo function for 4 choices
void histo(int a, int b, int c , int d, int e, int max);//histo fucntion for 5 choices
int max_check(int a, int b, int c, int d);//find max when 4 values
int max_check(int a, int b, int c, int d, int e);//find max when 5 values

int main()
{
    //declare all variables
    int num_choices(0), a(0), b(0),c(0),d(0),e(0);
    int max_val(0);
    int num_questions(0);
    cout<<"Welcome to the Choice historgram!"<<endl;
    cout<<"How many questions were on the test?";
    cin>>num_questions;
    cout<<endl;


while (num_questions!=0){

    cout<<"Please enter the number of choices:";
    cin>>num_choices;
    cout<<endl;

    while((valid(num_choices))==1)
    {
    cout<<"You entered an invalid number of questions!!\n"<<"Please enter 4 or 5:";
    cin>>num_choices;
	}

    //if they use 4 choices it will ask how many people picked each, then puts
    // the values found in respective functions (histo and max value)
    if (num_choices ==4)
    {
        cout<<"How many people picked a?:";
        cin>>a;cout<<endl;
        cout<<"How many people picked b?:";
        cin>>b;cout<<endl;
        cout<<"How many people picked c?:";
        cin>>c;cout<<endl;
        cout<<"How many people picked d?:";
        cin>>d;cout<<endl;
        max_val=max_check(a,b,c,d);
        histo(a,b,c,d,max_val);
    }

    //if they use 5 choices it will ask how many people picked each, then puts
    // the values found in respective functions (histo and max value)
    else
    {
        cout<<"How many people picked a?:";
        cin>>a;cout<<endl;
        cout<<"How many people picked b?:";
        cin>>b;cout<<endl;
        cout<<"How many people picked c?:";
        cin>>c;cout<<endl;
        cout<<"How many people picked d?:";
        cin>>d;cout<<endl;
        cout<<"How many people picked e?:";
        cin>>e;cout<<endl;
        max_val=max_check(a,b,c,d,e);
        histo(a,b,c,d,e,max_val);
    }

num_questions--;
}
return(0);

}

int valid(int num_choice)//function to check valid input
{
 	if (num_choice >5)
    return(1);
    else if(num_choice<4)
    return(1);
}
int max_check(int a, int b, int c, int d)
{
    int def_max(0);

    if((a>b)&&(a>c)&&(a>d))//if A is higher then the rest, then it is max
        def_max=a;
    else if ((b>a)&&(b>c)&&(b>d))//if b is higher then the rest, then it is max
        def_max=b;
    else if ((c>a)&&(c>b)&&(c>d))//if c is higher then the rest, then it is max
        def_max=c;
    else if ((d>a)&&(d>c)&&(d>b))//if d is higher then the rest, then it is max
        def_max=d;
    else if (((a==b)&&(a>c)&&(a>d))||((a==c)&&(a>b)&&(a>d))||((a==d)&&(a>c)&&(a>d)))//if A is the same as any those are the Max
        def_max=a;
    else if (((b==a)&&(b>c)&&(b>a))||((b==c)&&(b>a)&&(b>d))||((b==d)&&(b>a)&&(b>c)))//if b is the same as any those are the Max
        def_max=b;
     else if (((c==a)&&(c>b)&&(c>a))||((c==b)&&(c>a)&&(c>d))||((c==d)&&(c>a)&&(c>b)))//if c is the same as any those are the Max
        def_max=c;
    else
        def_max=d;

    return (def_max);
}

int max_check(int a, int b, int c, int d, int e)
{
        int def_max(0);

    if((a>b)&&(a>c)&&(a>d)&&(a>e))//check to see if A is greater then the rest
        def_max=a;
    else if ((b>a)&&(b>c)&&(b>d)&&(b>e))
        def_max=b;
    else if ((c>a)&&(c>b)&&(c>d)&&(c>e))
        def_max=c;
    else if ((d>a)&&(d>c)&&(d>b)&&(d>e))
        def_max=d;
    else if (((a==b)&&(a>c)&&(a>d)&&(a>e))||((a==c)&&(a>b)&&(a>d)&&(a>e))||((a==d)&&(a>b)&&(a>c)&&(a>e))||((a==e)&&(a>b)&&(a>c)&&(a>d)))//check if A is equal to any if no Max was found, this will be the max if true.
        def_max=a;
    else if ((b==c)&&(b>a)&&(b>d&&(b>e))||((b==a)&&(c<b)&&(b>d)&&(b>e))||((b==d)&&(c<b)&&(b>a)&&(b>e))||((b==e)&&(c<b)&&(b>a)&&(b>d)))
        def_max=b;
    else if ((c==b)&&(c>a)&&(c>d&&(c>e))||((c==a)&&(c>b)&&(c>d)&&(c>e))||((c==d)&&(c>b)&&(c>a)&&(c>e))||((c==e)&&(c>b)&&(a<c)&&(c>d)))
        def_max=c;
    else if (((d==b)&&(d>c)&&(a<d)&&(d>e))||((d==c)&&(d>b)&&(a<d)&&(d>e))||((d==a)&&(d>b)&&(d>c)&&(d>e))||((d==e)&&(d>b)&&(d>c)&&(a<d)))
        def_max=d;
    else
        def_max=e;

    return (def_max);
}


void histo(int a, int b, int c , int d, int max)
{
    do{
    if (a>=max)
    {
        //prints X if the number in A is found to be greater then or equal to max
        cout<<"X";
    }
    if (b>=max)
    {
        cout<<"\tX";
    }
     else
     //prints a tab character if the X is not needed, this is for formatting reasons
    cout << "\t";
    if (c>=max)
    {
        cout<<"\tX";
    }
     else
    cout << "\t";
    if (d>=max)
    {
        cout<<"\tX";
    }
     else
    cout << "\t";
    max--;
    cout<<endl;
    }while(max>0);
    cout<<"-----------------------------------"<<endl;
    cout<<"A"<<"\tB"<<"\tC"<<"\tD"<<endl;
}

void histo(int a, int b, int c , int d, int e, int max)
{
    do{
    if (a>=max)
    {
        //prints an X if the value is equal to or greater then Max
        cout<<"X";
    }
    if (b>=max)
    {
        cout<<"\tX";
    }
     else
     //prints a tab character if the X is not needed, this is for formatting reasons
    cout << "\t";
    if (c>=max)
    {
        cout<<"\tX";
    }
     else
    cout << "\t";
    if (d>=max)
    {
        cout<<"\tX";
    }
     else
    cout << "\t";
    if (e>=max)
    {
        cout<<"\tX";
    }
     else
    cout << "\t";
    max--;//subtract 1 from the max to run the check again, will do this untill max ==0
    cout<<endl;
    }while(max>0);
    cout<<"----------------------------------"<<endl;
    cout<<"A"<<"\tB"<<"\tC"<<"\tD"<<"\tE"<<endl;
}

