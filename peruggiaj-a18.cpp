// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 18: <A game where you have to turn off all the lights
/*
    Purpose: <This program uses classes and multidimensional arrays.  The user will be able to choose a board in
    which he wants to play.  After they will turn off and on lights till all the lights are out.  I worked independent of anyone else.>
*/

#include<iostream>
#include <fstream>

using namespace std;

class LightsOut {
  public:
     LightsOut(); // constructor that sets the board to the inported file

     int get_numberOn() const;	// returns numberOn to make sure the user hasn't won.  Will
                                                // loop untill it is 0.

     void make_move(int r, int c);	 // when a user chooses to play at row r and column c
                                                    // will call the toggle private function for all surrounding spaces

     void display() const;  // displays the board on the screen

  private:
     bool gameboard[5][5];	// light on=true ( 1) ; light off=false (0)
     int numberOn;		// the number of lights on; the user wins game when numberOn=0
     void toggle(int r, int c);
     // when called with row r and column c
     // if  0 <= r <= 5 and 0<= c <= 5
     // this private mutator changes gameboard[r][c] from true to false or false to true
     // outside of these appropriate ranges, this mutator makes no change
};


int main()
{
    char again;
    do{ // start of game loop
    int r_change , c_change;  // variable for user input
    LightsOut board;  // make a board
    int left_on=board.get_numberOn();  // return lights on into a variable
    board.display();  // print the initial board

    while (left_on !=0)  //loop while there are lights to turn off
    {
        cout<<"Which Row and Column would you like to change? ( r ,  c) ";
        cin >>r_change >>c_change;
        board.make_move(r_change, c_change);  // call to class function
        left_on=board.get_numberOn();  // return lights on
    }

    cout <<"YOU WIN!!!\n";
    cout<<"Would you like to play again? ( y/ n) ";
    cin>>again;
}while (again != 'n' || again!= 'N');
}


LightsOut::LightsOut()
{
    char inputname[20]; int number;
    numberOn=0;  // sets lights on to 0
    ifstream input;  // sets input file stream
    cout<<"What is the file name of the board you wish to play? ";
    cin>>inputname;
    input.open(inputname);  // opens file
    while (input.fail()) // if file can't open will tell the user and will ask for a new one
                {
                    cout<<"Open "<<inputname<< " failed. Please try again\n"<<endl;
                    input.close();
                    input.clear();  // clears the input flag
                    cout<<"File Name: ";
                    cin>>inputname;
                    input.open(inputname);
                }
    for (int r = 0; r < 5; r++){ // while the rows are less then 5
        for (int c=0; c<5;c++){  // while the columns are less then 5
            input>>number;  // get a number
            gameboard[r][c]=number;  // place the number in the array at the proper location
            if (number==1)  // if the light is on
                numberOn++;
        }
    }
}

void LightsOut::display() const
{
    for (int r=0; r< 5; r++){  // row counter
        for (int c=0; c < 5; c++) // column counter
            cout <<gameboard[r][c]<<" "; // print the array location of each row , column
        cout<<endl;  // go to next line after row is over
    }
    cout <<"\nTotal Lights on:"<<numberOn<<endl;  // shows how many lights are left on still
}

int LightsOut::get_numberOn()const {
return numberOn;  // return the private member variable
}

void LightsOut::make_move(int r, int c){
    toggle(r, c);  // the users input
    toggle ((r+1), c); // one abouve users input
    toggle((r-1), c);  // one below users input
    toggle (r, (c+1));  // one to the right
    toggle(r, (c-1));  // one to the left
    display();  // display board after the changes.
}

void LightsOut::toggle(int r, int c)
{
    if ((0<=r)&&(r<5)&&(0<=c)&&(c<5)){  // while the inputs are valid
            if (gameboard[r][c]==true){  // if true make it false and subtract one from lights on
                gameboard[r][c]=false;
                numberOn--;
                }
            else{ // turn the light on and add one to lights on
                gameboard[r][c]=true;
                numberOn++;
                }
        }
}
