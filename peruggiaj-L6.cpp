#include<iostream>
#include <ctime>
#include<string>

using namespace std;

enum SUIT{CLUBS=1, DIAMONDS, HEARTS, SPADES};
enum RANK{ACE=1,TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING};

bool dealt_out=false; // global variable used for menu statments.

class Card{
    public:
        Card(); // constructor
        Card(SUIT suit, RANK rank);  // constructor with parameters
        const char get_type();
        //precondition: Card has been made
        //post condition: Will return the type of card in a symbol form
        const string get_rank();
        //precondition: Card has been made
        //postcondition: Will return the Rank of the card as a string
        void set_rank(RANK rank);
        //precondition: Object has been made
        //post condition: will set the rank of the card
        void set_type(SUIT suit);
        //precondition: Object has been made
        //postcondition: Will set the suit of the card
        friend ostream& operator << (ostream& outs, Card& out_object);

    private:
        SUIT suit_of_card;
        RANK rank_of_card;
};

void shuffle_deck(Card Deck[]);
//precondition: An array of Card objects has been made
//postconiditon: will shuffle the deck randomly

void make_card();

void deal_cards(const Card Deck[], Card players[][13], int to_deal);
// precondition: None
//postcondition: Will deal the cards to 4 people, user will specify how many cards per hand.
void print_hands(Card players[][13], int cards_hand);
//precondition: The cards have been delt.
//postcondition: Will print the hands to the screen
void home_menu();
//function to show user menu

void suit_menu(); // menu to show suit options
void rank_menu(); // menu to show rank options

int main()
{
    int index=0, choice(0), cards_hand(0);
    SUIT fillsuit;
    RANK fillrank;
    Card Deck[52]; // 52 card objects
    Card player_hands[4][13]; // multi dimensional array, 4 X 13, 4 players, max of 13 cards per

    for (int s=CLUBS; s<=SPADES; s++) // for loop, while there are still suits to be made
        for (int v=ACE; v<= KING; v++) // pass out cards 2 - Ace
        {
            fillsuit=static_cast<SUIT>(s); // get the enum back
            fillrank=static_cast<RANK>(v); // get the enum back
            Deck[index].set_type(fillsuit); // fill set card with suit
            Deck[index].set_rank(fillrank);// fill set card with rank
            index++;
        }

  while(choice < 6){ // while loop for menu and switch statement
    home_menu();
    cout<<"\nChoice: ";
    cin>>choice;
    switch (choice){
        case(1): // display deck
            for (int i=0; i < 52; i++)
                cout<<Deck[i]<<endl;
            break;
        case(2): // shuffle deck
            shuffle_deck(Deck);
            break;
        case (3):
            make_card();
            break;
        case(4): // deal cards out
            dealt_out=true;
            cout<<"How many cards per hand? ";
            cin>>cards_hand;
            deal_cards(Deck, player_hands, cards_hand); // call to deal function
            break;
        case(5):
            if (dealt_out==true){ // if cards are out, they can print hands
            print_hands(player_hands, cards_hand);
            break;}
            else{// they will see option 4 as exit
                exit(1);
                break;
            }
        case(6): // exit
            exit(1);
            break;
        default:
            break;
    }
  }

}

Card::Card(){
//left blank on purpose
}

Card::Card(SUIT suit, RANK rank){ // constructor w/ parameters.
    rank_of_card=rank;
    suit_of_card=suit;
}

void Card::set_type(SUIT suit){
    suit_of_card=suit; // set suit of card
}

void Card::set_rank(RANK rank){
    rank_of_card=rank; // set rank of card
}

ostream& operator << (ostream& outs, Card& object){
    outs<<object.get_rank()<<" of " <<object.get_type(); // will print the rank and suit of card
    return outs;
}

const string Card:: get_rank(){ // returns rank of card as a string

    switch(rank_of_card){
        case(ACE):
            return "Ace";
        case(TWO):
            return "Two";
        case(THREE):
            return "Three";
        case(FOUR):
            return "Four";
        case(FIVE):
            return "Five";
        case(SIX):
            return "Six";
        case(SEVEN):
            return "Seven";
        case(EIGHT):
            return "Eight";
        case(NINE):
            return "Nine";
        case(TEN):
            return "Ten";
        case(JACK):
            return "Jack";
        case(QUEEN):
            return "Queen";
        case(KING):
            return "King";
        default:
            return("");
    }
}

const char Card::get_type(){ // returns type of card as char symbol for selected suit
    switch(suit_of_card){
        case(CLUBS):
            return(5);
        case(DIAMONDS):
            return (4);
        case(HEARTS):
            return (3);
        case(SPADES):
            return (6);
        default:
            return(' ');
    }
}


void shuffle_deck(Card Deck[]){
    int randnum(0);
    Card Temp; // temp card value
    for (int place=0; place<52; place ++){ // does all 52 cards
                randnum=(rand()%52);  // randnum from 0 - 51
                Temp=Deck[place]; // takes random card and puts it in temp
                Deck[place]=Deck[randnum]; // takes place you are and puts random card there
                Deck[randnum]=Temp; // puts original card in that random place
        }
}

void home_menu(){ // menu print
    cout<<"|\tMENU\t|\n"<<"-----------------"<<endl;
    cout<<"1. Display the Deck\n"<<"2. Shuffle Deck\n"<<"3. Make your own card\n"<<"4. Deal Cards out\n";
    if (dealt_out == true)
        cout<<"5. Show each players hand\n"<<"6. Quit";
    else
        cout<<"5. Quit";
}

void deal_cards(const Card Deck[], Card players[][13], int to_deal){
        int spot_in_deck; // place holder
        while(to_deal<1 || to_deal>13 ) // error out if invalid number
        {
            cout<<"To many Cards per hand! Enter again: ";
            cin>>to_deal;
        }

    for (int player =0 ; player<4; player ++) // while there are still players that need hands
        for (int cards=0; cards<to_deal; cards++){// while there are cards per hand still
            players[player][cards]=Deck[spot_in_deck]; // give player cards
            spot_in_deck++;// increase spot in orignal deck by 1 each time a card is delt
        }
}

void print_hands(Card players[][13], int cards_hand){ // print player hands
    string p1value, p2value, p3value, p4value;
    char p1type, p2type, p3type, p4type;
    cout<<"Player 1\t"<<"Player 2\t"<<"Player 3\t"<<"Player 4\t"<<endl;
    cout<<"________________________________________________________"<<endl;

    for (int card_num=0; card_num<cards_hand; card_num++){
        // print the value and type for each player in a column format
        cout<<players[0][card_num]<<"\t"
                <<players[1][card_num]<<"\t"
                <<players[2][card_num]<<"\t"
                <<players[3][card_num]<<endl;
    }
    cout<<endl<<endl;
}

void make_card(){
    int suit_choice(0), rank_choice(0);
    RANK rank;
    SUIT suit;

    while(suit_choice <1 || suit_choice > 4){
    cout<<"Please select a Suit\n";
    suit_menu();
    cin>>suit_choice;
    switch (suit_choice){
        case (1):
            suit=HEARTS;
            break;
        case (2):
            suit=CLUBS;
            break;
        case(3):
            suit=DIAMONDS;
            break;
        case(4):
            suit=SPADES;
            break;
        default:
            break;
        } // close switch statment
    } // end loop

   while(rank_choice <1 || rank_choice>13){
        cout<<"Please choose a rank\n";
        rank_menu();
        cin>>rank_choice;
        switch(rank_choice){
            case (1):
                rank=ACE;
                break;
            case(2):
                rank=TWO;
                break;
            case(3):
                rank=THREE;
                break;
            case(4):
                rank=FOUR;
                break;
            case(5):
                rank=FIVE;
                break;
            case(6):
                rank=SIX;
                break;
            case(7):
                rank=SEVEN;
                break;
            case(8):
                rank=EIGHT;
                break;
            case(9):
                rank=NINE;
                break;
            case(10):
                rank=TEN;
                break;
            case(11):
                rank=JACK;
                break;
            case(12):
                rank=QUEEN;
                break;
            case(13):
                rank=KING;
                break;
            default:
                break;
        }// end switch statment
   }//end loop

    Card player_made(suit, rank);
    cout<<"You made a Card.  Your card is:\n "<<player_made<<endl<<endl;
}

void suit_menu(){
    cout<<"\n1. Hearts\n"<<"2. Clubs\n"<<"3. Diamonds\n"<<"4. Spades\n"<<"Choice: ";
}

void rank_menu(){
    cout<<"\n1. Ace\n"<<"2. Two\n"<<"3. Three\n"<<"4. Four\n"<<"5. Five\n"<<"6. Six\n"<<"7. Seven\n"<<"8. Eight\n"
            <<"9. Nine\n"<<"10. Ten\n"<<"11. Jack\n"<<"12. Queen\n"<<"13. King\n"<<"Choice: ";
}
