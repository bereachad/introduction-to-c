// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia  (or both names if you are working with a partner)
// Assignment 12: <Allows the user to input 2 different points..  It will then calculate the difference between them>
/*
    Purpose: <This program will take two points entered by the user, containing both x and y componenets and tell you if they are the same
                        what the distance is between them and echo the points as well in a nicely formated output screen.>
*/
#include<iostream>
#include<cmath>

// Here we define a new compound data type called point.
struct point {
  double x;
  double y; // point has two member variables.
};

using namespace std;

void printPoint( point p ); // print function to print the cords of a point
point inputPoint(); // allows the user to input a point
bool pointEqual(point p1, point p2); // boolean function to check if points are the samee
double distance_from(point p1, point p2); // calculates the distance between two points
bool sameLine(point p1, point p2, point p3);

int main()
{
double distance_between(0); // declare variable that will be the distance between them to store what the distance _from function returns.
cout<<"Welcome to the Point distance Calcualator"<<endl<<endl;

point p1=inputPoint();  // point 1 will be inputPoint functions return
point p2=inputPoint(); // point 2 will be inputPoint functions return

if (pointEqual(p1, p2)) {// points are equal
    cout<<"The two points are the same, it is not a valid input";
    cout<<"Point 1 was: ";
    printPoint(p1);
    cout<<endl;
    cout<<"Point 2 was: ";
    printPoint(p2);
    cout<<endl;
  }
else{
    distance_between= distance_from (p1, p2);
    cout<<"Point 1 was: ";
    printPoint(p1);
    cout<<endl<<"Point 2 was: ";
    printPoint(p2);cout<<endl;
    cout<<"The distance between the two points was "<<distance_between<<endl<<endl;
    cout<<"Enter a third point to see if they are on the same line!!"<<endl<<endl;
    point p3=inputPoint();
            if (sameLine(p1,p2,p3))
            {
                cout<<"The points are on the same line!!"<<endl;
            }
            else
                cout<<"They do not lie on the same line!!"<<endl;

}

system("PAUSE");
return 0;
}

double distance_from(point p1, point p2){
double dis(0), x(0), y(0);
y=((p2.y)-(p1.y)); // gets change in y
y=pow(y,2); // raise it to the 2nd power
x=((p2.x)-(p1.x)); // gets change in x
x=pow(x,2); // raise it to 2nd power
dis=x+y; // adds x and y
dis=sqrt(dis); // square root of  x^2+y^2
return dis;
}

point inputPoint( ){
point p; // point variable set to return
cout<<"Enter the x cord:";
cin>>p.x; // enter p's x cord
cout<<"Enter the Y cord:";
cin>>p.y; // enter p's y cord
cout<<endl;
return p;
}

void printPoint( point p ) {
  cout << "(" << p.x << ", " << p.y << ")";
}

bool pointEqual(point p1, point p2){
return ((p1.x==p2.x) && (p1.y==p2.y)); // if the two points have the same X and Y
}

bool sameLine(point p1, point p2, point p3){

double slope_p1_p2;
double slope_p2_p3;
slope_p1_p2=((p2.y-p1.y)/(p2.x-p1.x));
slope_p2_p3=((p3.y-p2.y)/(p3.x-p2.x));
return ((slope_p1_p2==slope_p2_p3));
}
