
// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia 
// Assignment A4: Nesting the C++ Conditional Statement 
// Purpose: This program reads in a year as input and outputs the 
// the number of days in that year in an easily readable format


#include <iostream>
using namespace std;


int main()

{
    int year_input(0); 
    int year_mod(0);
    char repeat;
    
    cout<<"This program will allow you to figure out if a year was a leap year"<<endl;
    cout<<endl<<endl;
 //set up a repeat loop if user wants to test more then 1 year.   
do
{
    //ask user to input year
    cout<<"Please input a year (XXXX):";
    cin>>year_input;
    
    //checks to see if valid year first
    if(year_input<1582)
       {
             cout<<"Please enter a Valid year after 1582"<<endl;
       }
    else
        //if the year is /400 evenly, then it is a leap year.
        if((year_input%400==0))
        {
             cout<<"The year entered is a leap year!"<<endl;
             cout<<"There are 366 days in that year"<<endl;
        }
        //if the year is not divisable by 400, but is by 100, it is not a leap year
        else if (year_input%100==0)
        {
             cout<<"The year entered is not a leap year!"<<endl;
             cout<<"There are 365 days in that year"<<endl;            
        }
        //if the year can be divided by 4 evenly, and doesnt fall under the above
        //conditions, it is a leap year.
        else if (year_input%4==0)
        {
             cout<<"The year entered is a leap year!"<<endl;
             cout<<"There are 366 days in that year"<<endl;
            
        }
        else 
        //anything else is not a leap year
        {
             cout<<"This year is not a leap year!"<<endl;
             cout<<"There are 365 days in that year!"<<endl;
             cout<<endl;
        }
    cout<<"Would you like to input a new year? (Y/N)";
    cin>>repeat;
}while(repeat=='Y' || repeat=='y');       
    system("PAUSE");
    return(0);

}
         
