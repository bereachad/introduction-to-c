// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 7: <Will calculate area's of various rectangles.>
/*
    Purpose: <Will allow a user to input values of different type as the sides of a rectangle, either double, or integer as well
    as coordinates of the two corners of one to find the area.  It does the same for a square.>
*/

#include <iostream>
# include <cmath>
using namespace std;

double areaRect(double width, double height);
int areaRect(int width, int height);
double areaRect(double dimension);
int areaRect(int dimension);
double areaRect(double x1, double y1, double x2, double y2);

int main()
{
    double rwidth, rheight,swidth;
    int r_width, r_height, s_width;
    double x1,y1,x2,y2;

    cout<<"Please input the width and height of a rectangle format of XX.X: ";
    cin>>rwidth>>rheight;
    cout <<endl;
    cout<<"The Area of this rectange is  "<<areaRect(rwidth,rheight)<<"  units^2"<<endl;
    cout<<"Please input the width and height of a rectange in whole numbers only: ";
    cin>>r_width>>r_height;
    cout<<endl;
    cout<<"The Area of this rectange is  "<<areaRect(r_width,r_height)<<"  units^2"<<endl;
    cout<<"Please input the dimension of the square format of XX.X: ";
    cin>>swidth;
    cout<<endl;
    cout<<"The Area of this rectange is  "<<areaRect(swidth)<<"  units^2"<<endl;
    cout<<"Please input the dimenstions of the square as a whole number: ";
    cin>>s_width;
    cout<<endl;
    cout<<"The Area of this rectange is  "<<areaRect(s_width)<<"  units^2"<<endl;
    cout<<"Please input the cordinates coordinates of two opposite corners of the rectangle: "<<endl;
    cout<<"x1: ";cin>>x1;
    cout<<"y1: ";cin>>y1;
    cout<<"x2: ";cin>>x2;
    cout<<"y2: ";cin>>y2;
    cout<<endl;
    cout<<"The Area of this rectange is  "<<areaRect(x1,y1,x2,y2)<<"  units^2"<<endl;

    system("PAUSE");
    return(0);
}

double areaRect(double x1, double y1, double x2, double y2)
{
    double changex = x2 -x1;
    double changey=y2-y1;
    double area = changex*changey;
    area = abs(area);
    return(area);
}

double areaRect(double width, double height)
{
    double area=width*height;
    return (area);
}

int areaRect(int width, int height)
{
    int area=width*height;
    return(area);
}
double areaRect(double dimension)
{
    double area=pow(dimension,2.0);
    return (area);
}

int areaRect(int dimension)
{
    int area=pow(dimension,2.0);
    return (area);
}
