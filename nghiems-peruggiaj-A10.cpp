// Course: CSC 226 Introduction to Programming with C++
// Name: Son Nghiem (driver) & James Peruggia (navigator).
// Assignment 10: Input from and Output to files.
/*
    Purpose: a program that reads an unspecified number of real numbers
    from a file, finds the maximum, the minimum, and computes the average.
    The program then outputs these numbers both to the screen and to an output file.
*/

#include <iostream> // for cin, cout
#include <fstream> // for I/O member functions
#include <cstdlib> // for the exit function
#include <cfloat> // for DBL_MAX
#include <iomanip> // for the setw function
using namespace std;

void read_n_echo(ifstream& inputstream, ofstream& outputstream);
// precondition: the streams inputstream & outputstream have been connected to files using the function open
// postcondition: Read all the real numbers from the data input stream.
// Echo each of the numbers read from the input stream onto the screen
// in easy to read format with 5 decimal points of accuracy
// Output max, min, average, the number of inputs
// in easy to read format with 5 decimal points of accuracy to both the output file and to the screen.

int main()ifstream& inputstream, ofstream& outputstream)
{
    ifstream input;
    ofstream output;

    char inputname[16], outputname[16]; // files' names can have at most 15 chars.

    cout << "Please enter the input file's name: ";
    cin >> inputname;
    cout << "Please enter the output file's name: ";
    cin >> outputname;

    // Condensed input and output file opening and checking.
    input.open(inputname);
    output.open(outputname);
    if (input.fail() || output.fail())
       {  cout << "Input or output file opening fails.";
          exit(1); }
    else read_n_echo(input, output); // Call the function.

    input.close();
    output.close();
    system("PAUSE");
    return 0;
}

void read_n_echo(ifstream& inputstream, ofstream& outputstream)
{
     double number, max, min, ave, sum;
     int count;

     min = DBL_MAX;      //set the min at maximum value.
     max = -DBL_MAX;     //set the max at negative maximum value.
     sum = 0;
     count = 0;

     // Set the format with 5 decimal points of accuracy on the screen.
     cout.setf(ios::fixed);
     cout.setf(ios::showpoint);
     cout.precision(5);
     cout << "The inputed numbers:\n";

     while (inputstream >> number)
     {
           count++;
           sum += number; // Add number to the sum.
           if (max < number) max = number;
           if (min > number) min = number;
           cout <<  number << endl;
     }
     ave = sum / count; // Calculate the average number.
     // Write to the screen.
     cout << "\nThere are " << count << " numbers." << endl
          << "The average: " << ave << endl
          << "The maximum: " << max << endl
          << "The minimum: " << min << endl;

     // Set the format with 5 decimal points of accuracy in the file.
     outputstream.setf(ios::fixed);
     outputstream.setf(ios::showpoint);
     outputstream.precision(5);
     // Write to the file.
     outputstream << "There are " << count << " numbers." << endl
                  << "The average: " << ave << endl
                  << "The maximum: " << max << endl
                  << "The minimum: " << min << endl;

}

