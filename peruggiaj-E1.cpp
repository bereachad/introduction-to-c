#include<iostream>
#include<cmath>

using namespace std;

bool validnum(int input, int& twent, int& three);
void welcome();

int main()
{

int user_input(0), twent(0), three(0);

welcome();
cout<<"Please enter an amount of money you would wish to recieve: ";
cin>>user_input;
cout<<endl;

if (validnum(user_input, twent, three))
    {
        cout<<"You requested: "<<user_input<<endl;
        cout<<"This is "<<twent<<" twenties and "<<three<<"Threes";
        cout<<endl;
    }
else
    {
        cout<<"Amount is not valid";
        cout<<endl;
    }

//system("PAUSE");
return(0);
}


void welcome()
{
    cout<<"Welcome to the Dollop ATM! We will tell you if your requested amount of money is valid"<<endl;
}

bool validnum(int input, int& twent, int& three)
{
    int check(0), rem_val(0);
    if ((input%20==0))//number is divisible by 20, no 3's are needed.
        {
            twent=(input/20);
            return (true); // number is valid
        }
    else if (input%3==0)//number is divisible by 3 after checking to see if it can be divided by 20 evenly.  no 20's are needed.
        {
            three= (input/3);
            return(true);//number is valid
        }
    else
    {
        int x=20;
        do { //for loop to divide by incriments of 20 till a multiple of 3 is found.  If none is found, it will return false.
                check=input-x;
                rem_val=check%3;
                if (rem_val==0)
                {
                    twent = x/20; // num of twenties is x divide by 20
                    three= check/3;//num of three is remander of money after the 20's are taken out divided by 3;
                    return (true);
                    break;
                }
                x=x+20;
            }while(x<=input);
        return(false);//the value can't have a number of 20's and 3's that will make it even.
    }


}
