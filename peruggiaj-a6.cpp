// Course: CSC 226 Introduction to Programming with C++
// Name: Chad Peruggia
// Assignment 6: Temp Conversion
/* Purpose: <This program will allow a user to input a type of temp and will
    convert it to its corresponding value.  If the temp falls under certain conditions
    it will show you what it will feel like outside by factoriing the heat index
    or the wind chill.>*/
#include <iostream>
#include <cmath>
using namespace std;

int check_input(char user_input);
//declare functions
double c_to_f(double ctemp);double f_to_c(double ftemp);
void wind_chill(double ftemp);void heat_index(double ftemp);

int main()
{
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    double ftemp(0),ctemp(0);
 	char type_of_temp;

 	cout<<"Welcome to the Temperature Program"<<endl<<endl;
 	cout<<"What type of temperature would you like to enter? (C / F):";
 	cin>>type_of_temp;
 	cout<<endl;
//loop to make sure the user selects a valid choice
 	while (check_input(type_of_temp)==1){
 		  cout<<"Please enter a valid format (C / F):";
		  cin >>type_of_temp;}
//meaning entered a C degree, will call the c_to_f function
 	if (check_input(type_of_temp)==2)
 	{cout<<"Enter the Temp:";
  	    cin>>ctemp;
  	    cout<<endl;
 	    ftemp=c_to_f(ctemp);
      	cout<<"You entered a Temp of: "<<ctemp
            <<" C"<<endl;
            cout<<"This converts to:"<<ftemp<<"  F"<<endl;
 	    if (ftemp>=80)
	 	   heat_index(ftemp);
	 	else if (ftemp<=50)
	 		 wind_chill(ftemp);}
 //meaning user entered a F option, will call the f_to_c function
 	else if(check_input(type_of_temp)==3)
 	{
 	    cout<<"Enter the Temp:";
 	    cin>>ftemp;
 	    cout<<endl;
 	    ctemp=f_to_c(ftemp);
 	    cout<<"You entered a Temp of: "<<ftemp
            <<" F"<<endl;
            cout<<"This converts to:"<<ctemp<<"  C"<<endl;
		if (ftemp>=80)
	 	   heat_index(ftemp);
 	    else if (ftemp<=50)
 	        wind_chill(ftemp);
	}
//system("PAUSE");
return (0);
}

//this function is to check the input, if the input is not valid will prompt to
//enter a new one, when it is valid assigns it a value to use in teh main function
int check_input(char user_input)
{
    int senario(0);
    if ((user_input=='c')||(user_input=='C')){
            senario=2;
            return (senario);}
    else if ((user_input=='f')||(user_input=='F')){
		 	senario=3;
            return (senario);}
    else {
	 	 senario=1;
	 	 return(senario);}
}
//This function converts from Cel to Farh
double c_to_f(double ctemp)
{
    const double convfact=1.8;
    double ftemp;
    ftemp=(convfact*ctemp)+32.0;
    return (ftemp);
}
//This function converts from Farh to Cel
double f_to_c(double ftemp){
    const double convfact=1.8;
    double ctemp;
    ctemp=(ftemp-32.0)/convfact;
    return (ctemp);}
//Computes the windchill temp and prints it
void wind_chill(double ftemp)
{
    double wind(0.0);
    cout<<"Please Enter the wind speed: ";
    cin>>wind;
    if (wind>=3){
    double chill=((35.73+(.6215*ftemp))-(35.75*pow(wind,0.16))+(0.4275*ftemp*pow(wind,0.16)));
    cout<<endl<<"With a wind chill it will feel like: "<<chill<<"  F outside!!"<<" Better bundle up!"<<endl;
	}
	else
	{cout<<"The wind will not affect the temp, it will feel the same as if there was no wind"<<endl;}
}

//computes the heat_index temp and will print it
void heat_index(double ftemp)
{
 	 long double humid;
 	 double percent;
 	 cout<<"Please Enter the Humidity: ";
 	 cin>>percent;
 	 humid=(-42.379+(2.04901523*ftemp))+(10.14333127*percent)-(.22475541*ftemp*percent);
 	 humid=humid-(.00683783*pow(ftemp,2))-(.05481717*pow(percent,2));
 	 humid=humid+((.00122874*pow(ftemp,2)*percent)+((.00085282*ftemp*pow(percent,2))));
 	 humid=humid-((.00000199*pow(ftemp,2)*pow(percent,2)));
 	 cout<<endl<<"Expect it to feel like "<<humid<<" F outside"<<endl;
 	 if (humid>=130)//if it is really hot
 	 	cout<<"Extreme danger-heat stroke is likely with continued exposure"<<endl;
}
