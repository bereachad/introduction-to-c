// Course: CSC 226 Introduction to Programming with C++
// Name: Matthew Ondak  Chad Peruggia
// Assignment 20: <This program will allow a user to enter a template letter, a data file and an output.
//                                  file name.  The template will be filled with the data and printed to an individual output file>
/*
    Purpose: <This program uses strings as well as arrays of strings.  We scan in a template and fix the template
                        with data from the data file.  Each letter will be filled with different data.  Teh output file names will
                        increment with what letter you are on, starting with 0.  Stringstream are used to simplify tokenization of the strings.>
*/
#include<iostream>
#include<fstream>
#include <string>
#include <cctype>
#include <cstdlib>
#include <sstream>
using namespace std;

const int DATA_TO_CHANGE=4;

void print (ostream& output, string Letter[], const int count);
//precondition: the letter has been changed and populated
//postcondition:  will print the letter with changes made to various different files.

void recopy(string Letter[], string cLetter[], const int count);
//precondition: original letter has been loaded in
//postcondition: there will be a copy of the first letter made to work with

void replace_stuff(string& Letter,  string data[][4], const int datarow);
//precondition: The data file has been read in corresponding to the letter we are on.
//postconditon: all fields in the letter have been properly replaced and fixed.

void fill_data(string dataLines[][4], ifstream& input,int& counter);
//precondtion: a valid data file name has been entered
//postcondition: fills a string array with the data needed for each letter.  Clears and resets after each letter is done

string file_names(string name, int letter_number);
//precondtion: a valid output file name has been entered
//postcondition: changes the file name to allow the letters to be printed on different files

int main()
{
    char letter_name[20], data_name[20];  // c-strings to allow user input for file names
    int count(0), datacount(0), totaldataloops(0);  // various variables used to count loops
    string tempstring, outstring;  // strings used, outstring is for output file, countstring is a temp string holder
    string orignalLetter[100], dataLines[100][DATA_TO_CHANGE],copyLetter[100];  // delcare string arrays
    ifstream input; ofstream out;  // opening streams

    cout<<"What is the file of the letter which you want to work with? ";
    cin>>letter_name;
    input.open(letter_name);

    while(input.fail())
    {
        cout<<"The file failed to open, please enter a new one:";
        input.close();
        input.clear();  // clear flags to attempt to reopen
        cin>>letter_name;
        input.open(letter_name);
    }

    while (getline(input,orignalLetter[count]))  // while the stream can get lines, terminating with "\n" they will be put into an array one line at a time
        count++;

    input.close(); // close orignal letter from input stream
    input.clear(); // clear all flags.

    recopy(orignalLetter, copyLetter, count);    // copy letter to work with it

    cout<<"What is the name of the data file?";
    cin >> data_name;
    input.open(data_name);
    fill_data(dataLines, input, totaldataloops); // pass string array, inputstream, and number of lines in the datafile
    input.close();

    cout <<"What is the outputfile? :";
    cin>>outstring;
    tempstring = outstring; // copy original name of cout file so we can compare it multiple times

    while (datacount < totaldataloops){  // while the loop count you are on is less then the total data segments
            //function call to replace while there are still lines to check:
            // once lines = count, we know that the whole letter has been checked.
            for (int lines=0; lines <count; lines++){
                        replace_stuff(copyLetter[lines], dataLines, datacount);
                    }
            print(cout, copyLetter, count);  // print the letter to the screen
            outstring=file_names(tempstring, datacount);  // names outstring to new file names  **EXTRA CREDIT**
            out.open(outstring.c_str());  // opens the stream using a cstring
            print (out, copyLetter, count);  // prints to the file
            out.close();  // close output
            out.clear();
            recopy (orignalLetter, copyLetter, count);  //copy letter again to start over
            datacount++; // add one to data count looper
        }
    system("PAUSE");
    return (0);
}

void recopy(string Letter[], string cLetter[], const int count)
{
    for (int i=0; i < count; i++)
        cLetter[i]=Letter[i];  // copys the letter till they are the same
}

void fill_data(string dataLines[][4], ifstream& input,int& counter)
{
    string temp;
   while(!input.eof())  // while there are still lines of data to be read
   {
        getline(input, temp);  // get the next line and make it string temp '\n' being the deliminater
        stringstream ss; // setup a stringstream to handle the string as a stream
        ss << temp;
        for (int c=0; c<4; c++)
            getline(ss, dataLines[counter][c],'\t');  // while the rows arent full, stream in the line, using '\t' as the deliminator
    counter++;
   }
}

void replace_stuff(string& Letter, string data[][4], const int datarow)
{
    int location_fix(0);int dataIndex; // variables
    int line_length = Letter.length();  // get the length of the line you are on
    while( location_fix<line_length)  // location you are on is less then the line length
    {
        location_fix = Letter.find("<", location_fix); // finds the first <
        if ((location_fix >= 0) && (location_fix < line_length))  // if it finds it between the line length, it will work!
        {
            dataIndex=((Letter[location_fix+1])-'0');// the number after the <, since character, we subtract '0' to get it to an int
            Letter.erase(location_fix, 3); // erase the < number and >
            Letter.insert(location_fix, data[datarow][dataIndex]); // puts in the data array for either 1 - 4 take 48 away due to ANSCI table
            line_length=Letter.length();  // get the new length after you place your fix in for the loop
        }
        else//nothing to fix on this line, break out of the loop
            break;
    }
}

void print(ostream& outs, string Letter[], const int count)
{
    for (int x=0; x < count; x++)  // prints one line at a time, till there is no lines left
        outs << Letter[x]<<endl;
}

string file_names(string name, int letter_number)  // function to change output file name
{
    int pos(0), length = name.length();
    string finalname;
    pos=name.find(".",pos); // set position to the spot where the start of the file extension is
    if (pos >0 && pos <=length){  // there was a file extension entered
        name.erase(pos); // erase everything after the '.'
        stringstream ss;  // opens a stringstream
        ss<<name<<letter_number<<".txt";  // puts the name user entered, the integer of the dataline and .txt into a string
        finalname=ss.str(); // finalname is now the string of the stream
    }
    else {  // the user didn't type an extenstion on the file nam
        stringstream ss;  // opens a stringstream
        ss<<name<<letter_number<<".txt";  // puts the name user entered, the integer of the dataline and .txt into a string
        finalname=ss.str(); // finalname is now the string of the stream
    }
    return finalname;  // return that file name
}
